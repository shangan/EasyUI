package org.colin.controller;

import org.colin.controller.base.BaseControllerTest;
import org.colin.model.ro.SysResourceRO;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ResourceControllerTest extends BaseControllerTest {

	@Test
	public void testInsert() throws Exception {
		SysResourceRO obj = new SysResourceRO();
		obj.setResourceType(1);// 资源类型(0：菜单；1：按钮；2：权限资源；)
		obj.setParentId("5e1ff82adfb1acd28dbd1b26");// 父节点ID
		obj.setNameCn("修改系统参数");
		obj.setUrl("/config/update");
		obj.setSortNo(4);
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/resource/insert").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content(JSONObject.toJSONString(obj));
		// request 就是MockHttpServletRequestBuilder类
		request.header("token", "20200116133126620a042277c3ca4400aa5af567bf1d0c158");
		MvcResult mvcResult = mvc.perform(request).andReturn();
		// 获取状态码
		int status = mvcResult.getResponse().getStatus();// 500状态码 302状态码 404状态码 200状态码等
		log.info("status={}", status);
		// 获取返回 @ResponseBody json字符串 : 进行反序列化处理即可
		// 注意: 500/400/302则是返回的HTML源码String类型
		String contentAsString = mvcResult.getResponse().getContentAsString();
		log.info("contentAsString={}", contentAsString);
	}

}
