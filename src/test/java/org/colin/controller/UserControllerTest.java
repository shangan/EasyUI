package org.colin.controller;

import org.colin.controller.base.BaseControllerTest;
import org.colin.model.entity.SysUser;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserControllerTest extends BaseControllerTest {

	@Test
	public void testInsert() throws Exception {
		SysUser obj = new SysUser();
		obj.setLoginName("test1001");
		obj.setLoginPass("123456");
		obj.setUserName("测试账号1001");
		obj.setMobile("18086290000");
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/user/insert").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content(JSONObject.toJSONString(obj));
		// request 就是MockHttpServletRequestBuilder类
		request.header("token", "20200116133126620a042277c3ca4400aa5af567bf1d0c158");
		MvcResult mvcResult = mvc.perform(request).andReturn();
		// 获取状态码
		int status = mvcResult.getResponse().getStatus();// 500状态码 302状态码 404状态码 200状态码等
		log.info("status={}", status);
		// 获取返回 @ResponseBody json字符串 : 进行反序列化处理即可
		// 注意: 500/400/302则是返回的HTML源码String类型
		String contentAsString = mvcResult.getResponse().getContentAsString();
		log.info("contentAsString={}", contentAsString);
	}

}
