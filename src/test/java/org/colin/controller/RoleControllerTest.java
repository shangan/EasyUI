package org.colin.controller;

import java.util.ArrayList;
import java.util.List;
import org.colin.controller.base.BaseControllerTest;
import org.colin.model.ro.RoleResourceRO;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RoleControllerTest extends BaseControllerTest {

	@Test
	public void testInsert() throws Exception {
		RoleResourceRO obj = new RoleResourceRO();
		obj.setRoleId("5e0eae6e31c799d2d5ebf554");
		List<String> resourceIds = new ArrayList<String>();
		resourceIds.add("5e0ea7d731c72ae05abae2b2");
		resourceIds.add("5e0ea82b31c72ae05abae2b4");
		resourceIds.add("5e0ea86d31c72ae05abae2b6");
		resourceIds.add("5e0ea88931c72ae05abae2b8");
		resourceIds.add("5e0ea89931c72ae05abae2ba");
		resourceIds.add("5e0ea8be31c72ae05abae2bc");
		resourceIds.add("5e0ea90931c72ae05abae2be");
		resourceIds.add("5e0ea93031c72ae05abae2c0");
		resourceIds.add("5e0ea94931c72ae05abae2c2");
		resourceIds.add("5e0ea95831c72ae05abae2c4");
		resourceIds.add("5e0ea96631c72ae05abae2c6");
		resourceIds.add("5e0eadcd31c799d2d5ebf54a");
		resourceIds.add("5e0eadf131c799d2d5ebf54c");
		resourceIds.add("5e0eadfc31c799d2d5ebf54e");
		resourceIds.add("5e0eae2131c799d2d5ebf550");
		resourceIds.add("5e0eae3031c799d2d5ebf552");
		obj.setResourceIds(resourceIds);
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/role/updateRoleResource").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content(JSONObject.toJSONString(obj));
		// request 就是MockHttpServletRequestBuilder类
		request.header("token", "20200116133126620a042277c3ca4400aa5af567bf1d0c158");
		MvcResult mvcResult = mvc.perform(request).andReturn();
		// 获取状态码
		int status = mvcResult.getResponse().getStatus();// 500状态码 302状态码 404状态码 200状态码等
		log.info("status={}", status);
		// 获取返回 @ResponseBody json字符串 : 进行反序列化处理即可
		// 注意: 500/400/302则是返回的HTML源码String类型
		String contentAsString = mvcResult.getResponse().getContentAsString();
		log.info("contentAsString={}", contentAsString);
	}

	@Test
	public void testGetResourceByRoleId() throws Exception {
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/role/selectResourceListByRoleId");
		request.param("roleId", "5e0eae6e31c799d2d5ebf554");
		// request 就是MockHttpServletRequestBuilder类
		MvcResult mvcResult = mvc.perform(request).andReturn();
		// 获取状态码
		int status = mvcResult.getResponse().getStatus();// 500状态码 302状态码 404状态码 200状态码等
		log.info("status={}", status);
		// 获取返回 @ResponseBody json字符串 : 进行反序列化处理即可
		// 注意: 500/400/302则是返回的HTML源码String类型
		String contentAsString = mvcResult.getResponse().getContentAsString();
		log.info("contentAsString={}", contentAsString);
	}
}
