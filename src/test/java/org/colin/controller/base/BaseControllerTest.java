package org.colin.controller.base;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class BaseControllerTest {

	// 注入web环境的ApplicationContext容器
	@Autowired
	protected WebApplicationContext webApplicationContext;

	/**
	 * 模拟mvc测试对象
	 */
	protected MockMvc mvc;

	/**
	 * 所有测试方法执行之前执行该方法
	 */
	@Before // 这个注解的作用,在每个方法之前都会执行一遍
	public void before() {
		// 获取mockmvc对象实例
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}
}
