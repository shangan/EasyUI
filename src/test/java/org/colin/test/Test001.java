package org.colin.test;

public class Test001 {

    public static void main(String[] args) {
        System.out.println(add(1, 2));
    }

    /**
     * @Description: 两个数相加
     * @MethodName: add
     * @param: [a, b]
     * @return: int
     * @Author: wujiangbo
     * @Date: 2020-01-06 17:12
     */
    public static int add(int a, int b) {
        return a + b;
    }

    /**
     * @Description: 的扩散到那你就
     * @MethodName: isTrue
     * @param: [s1, i1, i2]
     * @return: boolean
     * @Author: wujiangbo
     * @Date: 2020-01-07 17:28
     */
    public static boolean isTrue(String s1, int i1, int i2) {
        return true;
    }
}
