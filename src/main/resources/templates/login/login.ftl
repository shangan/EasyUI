<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="/static/images/favicon1.ico" rel="icon" type="image/x-icon"/>
    <link href="/static/images/favicon1.ico" rel="shortcut icon" type="image/x-icon"/>
    <title>OA办公自动化系统</title>
    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/static/css/login.css">
    <style>

        .inp {
            border: 1px solid #cccccc;
            border-radius: 2px;
            padding: 0 10px;
            width: 278px;
            height: 40px;
            font-size: 18px;
        }

        /*
                .btn {
                    display: inline-block;
                    box-sizing: border-box;
                    border: 1px solid #cccccc;
                    border-radius: 2px;
                    width: 100px;
                    height: 40px;
                    line-height: 40px;
                    font-size: 16px;
                    color: #666;
                    cursor: pointer;
                    background: white linear-gradient(180deg, #ffffff 0%, #f3f3f3 100%);
                } */

        .btn:hover {
            background: #3c83a5;
            color: #fff;
        }

        #captcha {
            width: 300px;
            display: inline-block;
        }

        label {
            vertical-align: top;
            display: inline-block;
            width: 80px;
            text-align: right;
        }

        #wait {
            text-align: left;
            color: #666;
            margin: 0;
        }

        .thisimg:HOVER {
            cursor: pointer;
        }

        .test:FOCUS {
            border-color: #66afe9 !important;
        }

        .modal-dialog {
            width: 500px;
        }

        .modal-body .icon {
            height: 80px;
            width: 80px;
            margin: 20px auto;
            border-radius: 50%;
            color: #aad6aa;
            border: 3px solid #d9ead9;
            text-align: center;
            font-size: 44px;
        }

        .modal-body .icon .glyphicon {
            top: 11px;
        }

        .modal-p {
            margin: 20px auto;
        }

        .modal-body .modal-p h2 {
            text-align: center;
        }

        .modal-body .modal-p p {
            text-align: center;
            color: #666;
            font-size: 16px;
            padding-top: 8px;
            font-weight: 300;
        }

        .modal-p .btn {
            width: 100px;
            height: 40px;
        }

        .modal-error .icon {
            color: #f27474;
            border: 3px solid #f27474;
        }
    </style>
</head>

<body>
<div class="cotn_principal">
    <div class="cont_centrar" style="top:5%;">
        <div class="cont_login">
            <div class="cont_forms cont_forms_active_login" style="height: 480px;width: 400px;border-radius: 15px;">
                <div class="cont_img_back_"><img src="/static/images/timg.jpeg" alt="背景图片"/></div>
                <form action="goLogin" method="post" onsubmit="return check();">
                    <div class="cont_form_login" style="display: block;opacity: 1;width: 400px;">
                        <div class="alert alert-danger alert-dismissible" role="alert"
                             style="position: absolute;padding: 11px;display: none">
                            错误信息: <span class="error-mess"></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"
                                    style="right:0px;"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <h2>OA办公自动化系统</h2>
                        <input type="text" placeholder="请输入登录帐号" autofocus="autofocus" value="${(userName)!''}"
                               name="userName" class="userName test"/>
                        <input type="password" placeholder="请输入登录密码" value="" name="passWord" class="password test"/>
                        <input type="text" placeholder="图片验证码" style="margin-bottom: 20px;width:152px" name="code"
                               class="code test"/>
                        <div class="login-img" style="display:inline-block">
                            <img class="thisimg" onclick="this.src='getPictureVerification?r'+Date.now()"
                                 src="getPictureVerification" alt="验证码"
                                 style="width: 100px;height: 42px;border-radius: 3px;">
                        </div>
                        <br>
                        <button class="btn_login btn" type="submit">登录</button>

                    </div>
                </form>
            </div>
            <div style="border: 1px solid transparent;">
                <p style="margin-top: 550px;font-weight: 400;color: #0c1535;font-size: 10px;letter-spacing:1px ;">
                    欢迎使用OA办公自动化系统</p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/static/easyui/jquery.min.js"></script>
<script type="text/javascript" src="/static/bootstrap/js/bootstrap.min.js"></script>

<#if errormess??>
<script>
    $(function(){
        $('.error-mess').text('${errormess}');
        $('.alert-danger').css('display','block');
    })
</script>
</#if>
<script type="text/javascript">
    function check() {
        var userName = $('.userName').val().trim();
        var password = $('.password').val().trim();
        var code = $('.code').val().trim();
        var count = 1;
        if (userName == null || userName == "") {
            $('.error-mess').text("登录账号不能为空!");
            $('.alert-danger').css('display', 'block');
            $('.userName').css('border-color', "#a94442");
            count = 0;
            return false;
        }
        if (password == null || password == "") {
            $('.error-mess').text("登录密码不能为空!");
            $('.alert-danger').css('display', 'block');
            $('.password').css('border-color', "#a94442");
            count = 0;
            return false;
        }
        if (code == null || code == "") {
            $('.error-mess').text("验证码不能为空!");
            $('.alert-danger').css('display', 'block');
            $('.code').css('border-color', "#a94442");
            count = 0;
            return false;
        }
        return true;
    }
</script>
</body>
</html>