/**
 * 资源列表的js文件
*/

$(function () {
    $("#tbList").treegrid({
        url: "/resource/menuInitEasyUI",
        method: 'post',
        title: '资源列表',
        idField: 'id',
        treeField: 'text',
        iconCls: 'ext-icon-application_side_tree',
        rownumbers: true,
        animate: true,
        fitColumns: true,
        resizable: true,
        frozenColumns: [[
            { title: '菜单名称', field: 'text', width: 200 }
        ]],
        columns: [[
        	{ title: '图标', field: 'icon', width: 80},
            { title: '排序', field: 'sortNo', width: 40 },
            { title: '权限字符串', field: 'permission', width: 80},
            { title: '资源链接', field: 'url', width: 80 },                    
            {
                field: 'resourceType', title: '资源类型', width: 25, align: 'center', formatter: function (colData) {
                    var colDataStr = "";
                	if(colData === 0){
                		colDataStr = "菜单";
                    }
                	else if(colData === 1){
                		colDataStr = "按钮";
                	}
                	else if(colData === 2){
                		colDataStr = "权限资源";
                	}
                	return colDataStr;
                }
            },
            { title: 'ParentId', field: 'parentId', hidden: true }
        ]],
        toolbar: [{
            text: "添加",
            iconCls: 'icon-add',
            handler: add
        }, '-', {
            text: "修改",
            iconCls: 'icon-edit',
            handler: modify
        }, '-', {
            text: "删除",
            iconCls: 'icon-remove',
            handler: remove
        }]      

    });
});

function add(){}
function modify(){}
function remove(){}