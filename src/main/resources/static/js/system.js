/**
 * 系统共用函数
*/

//打印Log调试信息
function wjbLog(value){
	console.log(value);
}

//进度条开启
function wjbProOn(msg){
	$.messager.progress({
		title: '进度条',
		text: msg
	});
}

//进度条关闭
function wjbProOff(){
	$.messager.progress('close');
}

/**
 * $.messager.show
 * 系统提示框（5秒后自动关闭）
*/
function wjbShow(value){
	$.messager.show({
		title: "温馨提示",//头部面板上显示的标题文本
		msg: value,//要显示的消息文本
		showSpeed: 300,//定义消息窗口完成显示所需的以毫秒为单位的时间。默认是 600
		showType: 'slide',//定义消息窗口如何显示。可用的值是：null、slide、fade、show。默认是 slide。
		timeout: 5000, //如果定义为 0，除非用户关闭，消息窗口将不会关闭。如果定义为非 0 值，消息窗口将在超时后自动关闭。默认是 4 秒。
		style:{
			right: '',
			top: document.body.scrollTop + document.documentElement.scrollTop,
			bottom: ''
		}
	});
}

/**
 * $.messager.alert
 * 系统提示框（需要确认关闭）
 */
function wjbAlert(value){
	$.messager.alert("提示", value, "info");
}

/**
 * $.messager.error
 * 系统错误提示框（需要确认关闭）
 */
function wjbError(value){
	$.messager.alert("提示", value, "error");
}

/**
 * 跳转页面
 */
function goTo(url){
	window.location.href = url;
}

/**
 * form表单数据序列化
 */
function serializeObject(form){
    var o = {};
    $.each(form.serializeArray(), function(index){
      if(o[this['name'] ]){
          o[this['name'] ] = o[this['name'] ] + "," + this['value'];
      }else{
          o[this['name'] ]=this['value'];
      }
    });
  return o;
}

/**
 * 发送Ajax请求前触发
 */
$.ajaxSetup({
	dataType: "json",
    cache: false,
    type: "POST",
    headers: {
        "Token": document.cookie.split(";")[0]          //从cookie中读取token
    },
	complete: function (xhr) {
		console.log(xhr.responseJSON);
		if (xhr.responseJSON.code == "1000") {
			console.log("没有token");
			goTo("/login/login");
		}else{
			console.log("已经登录！");
		}
	}
});


