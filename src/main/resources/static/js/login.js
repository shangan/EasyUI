/**
 * 登录页面js
 */

var currentTime;

/**
 * 刷新验证码
 */
function refreshCode(){
	currentTime = new Date().getTime();
	document.getElementById("imgcode").src='/login/getPictureVerification?time=' + currentTime;
}
	
$(function(){
	refreshCode();
	$("#loginWindow").dialog({
		title : '系统登录',
		width : '460',
		height : '260',
		closable : false,
		minimizable : false,
		maximizable : false,
		resizable : false,
		modal : false,
		draggable : false,
		buttons : [ {
			text : '登录',
			iconCls : 'icon-ok',
			handler:function(){
				doLogin();
			}
		} ]
	});
	
	//登录操作
	function doLogin(){
		wjbProOn("正在登录中，请稍后...");
		var url = "/login/login";
		var form = $("#loginForm").form();
		var formJsonObject = serializeObject(form);
		formJsonObject.time = currentTime;//追加time属性
		$.ajax({
		    url: url,
		    type: "POST",     // 请求方式
		    async: true, //true:异步；false:同步
		    data: JSON.stringify(formJsonObject),//JSON.stringify(obj)这个函数可以把json对象转换为json字符串
		    cache: false,    //不要缓存
		    contentType: "application/json;charset=UTF-8",
		    dataType: "json",   // 预期服务器返回的数据类型
		    success: function(returnData){
		    	wjbProOff();
			    var code = returnData.code;
			    if(code != '000'){
			    	//操作失败
			    	wjbShow(returnData.message);
			    	refreshCode();
			    }else{
			    	//操作成功
			    	//js使用cookie保存token(cookie在http请求中，随着请求发送到服务器)
			    	document.cookie = returnData.data.map.token;
			    	//跳转至登录成功首页
			    	goTo("/login/loginSuccess");
			    }
			    console.log("systemToken="+systemToken);
		    },
			error: function(returnData){
				wjbError(returnData.message);
			}
		});
	}
});