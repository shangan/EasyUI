/**
 * 登录成功首页js
 */

/**
 * 刷新当前标签Tabs
 */
function RefreshTab(currentTab) {
    var url = $(currentTab.panel('options')).attr('href');
    $('#tabs').tabs('update', {
        tab: currentTab,
        options: {
            href: url
        }
    });
    currentTab.panel('refresh');
}

/**
 * 添加Tab
 */
function addTab(title, url, icon){
    if($('#tabs').tabs('exists',title)){
        $('#tabs').tabs('select',title);
        var currentTab = $('#tabs').tabs('getSelected');
        RefreshTab(currentTab);
    }else{
        var content = '<iframe scrolling="auto" frameborder="0"  src="' + url + '" style="width:100%;height:100%;"></iframe>';
        $('#tabs').tabs('add',{
	        title: title,
	        content: content,
	        closable: true,
	        iconCls: icon
        });
    }
}

$(function () {
    //实例化tabs组件
    $("#tabs").tabs({
        border: false,//无边框
        fit: true//铺满父容器
    });
    //添加一个选项卡
    $("#tabs").tabs('add',{
        title: "首页",
        content: "欢迎来到学习天地",
        closable: false//是否显示关闭按钮
    });
    
    //加载菜单
    $.ajax({
		type : 'POST',
		dataType : "json",
		url : '/resource/menuInitEasyUI',
		dataType: "json",   // 预期服务器返回的数据类型
		success : function(data) {
			$.each(data.data.list, function(i, n) {// 加载父类节点及一级菜单
				var id = n.id;
				wjbLog(n.children);
				$('#layout_west_accordion').accordion('add', {
					title : n.text,
					iconCls : n.iconCls,
					selected : true,
					content : '<div style="padding:10px"><ul id="tree-'+ id + '" name="'+n.text+'"></ul></div>',
				});  
				//解析整个页面
				$.parser.parse();
				//第二层生成树节点
				if (!n.children || n.children.length == 0) {
					return true;
				}
				$("#tree-" + id).tree({
					data : n.children,
					//animate : true,
					lines : true,// 显示虚线效果
					onClick : function(node) {
						if (node.url) {
							var tabTitle = node.text;
							var url = node.url;
							var icon = node.iconCls;
							addTab(tabTitle, url, icon);
						}
					}
				});
		});
	}
});
});
/*$("#menuTree").tree({
	url: "/resource/menuInit",
	method: 'post',//若不设置为get提交，会有405错误
	onClick: function (node){//node是触发的节点
		console.log(node);//打印显示node.attributes
		if (node.attributes){//如果有attributes对象,就进入
			var title = node.text;//树节点的文本
			var t = $("#tabs");//t为tabs标签
			if (t.tabs('exists',title)){//如果节点存在
				t.tabs('select', title)
			}
			else{//如果不存在，则添加节点
				var url = node.attributes.url;//节点url
				t.tabs('add', {
					title: title,//标题
					href: url,//地址来源
					closable: true
				});
			}
		}
	}
});*/