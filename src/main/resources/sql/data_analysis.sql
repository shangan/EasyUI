-- MySQL dump 10.13  Distrib 5.4.1-beta, for Win32 (ia32)
--
-- Host: localhost    Database: data_analysis
-- ------------------------------------------------------
-- Server version	5.4.1-beta-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sys_config`
--

DROP TABLE IF EXISTS `sys_config`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `sys_config` (
  `id` varchar(50) NOT NULL COMMENT '主键ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_user_id` varchar(50) DEFAULT NULL COMMENT '更新人ID',
  `key_str` varchar(50) DEFAULT NULL COMMENT 'key',
  `value_str` varchar(255) DEFAULT NULL COMMENT 'value',
  `description` varchar(255) DEFAULT NULL COMMENT '配置描述',
  `type` int(1) DEFAULT NULL COMMENT '类型',
  `label` varchar(50) DEFAULT NULL COMMENT '标签',
  `sort` int(5) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统配置表';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `sys_config`
--

LOCK TABLES `sys_config` WRITE;
/*!40000 ALTER TABLE `sys_config` DISABLE KEYS */;
INSERT INTO `sys_config` VALUES ('5e1ffe9cdfb1f22cd91f5001','2020-01-16 14:11:40','2020-01-16 14:13:36','1','companyName','武汉火星球科技有限公司','公司名称',1,'公司名称',1);
/*!40000 ALTER TABLE `sys_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_log`
--

DROP TABLE IF EXISTS `sys_log`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `sys_log` (
  `log_id` varchar(50) NOT NULL DEFAULT '' COMMENT '主键ID',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `type` varchar(10) NOT NULL COMMENT '日志类型（info/error）',
  `title` varchar(50) NOT NULL COMMENT '日志标题',
  `remote_addr` varchar(25) DEFAULT NULL COMMENT '请求地址',
  `request_uri` varchar(100) DEFAULT NULL COMMENT 'URI',
  `method` varchar(30) DEFAULT NULL COMMENT '请求方式',
  `exception` varchar(255) DEFAULT NULL COMMENT '异常',
  `operate_date` datetime DEFAULT NULL COMMENT '操作时间',
  `timeout` varchar(50) DEFAULT NULL COMMENT '执行时间（单位：毫秒）',
  `user_id` varchar(50) DEFAULT NULL COMMENT '用户ID',
  PRIMARY KEY (`log_id`),
  UNIQUE KEY `log_id` (`log_id`),
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统日志表';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `sys_log`
--

LOCK TABLES `sys_log` WRITE;
/*!40000 ALTER TABLE `sys_log` DISABLE KEYS */;
INSERT INTO `sys_log` VALUES ('5e21477edfb1d521e6178fc5','2020-01-17 13:34:54','info','用户管理-新增用户','0:0:0:0:0:0:0:1','/se/user/insert','POST','无异常','2020-01-17 13:34:54','103','1'),('5e214c82dfb1d18cbc239941','2020-01-17 13:56:19','info','用户管理-删除用户','0:0:0:0:0:0:0:1','/se/user/delete','POST','无异常','2020-01-17 13:56:18','549','1');
/*!40000 ALTER TABLE `sys_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_resource`
--

DROP TABLE IF EXISTS `sys_resource`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `sys_resource` (
  `id` varchar(50) NOT NULL DEFAULT '' COMMENT '资源id',
  `resource_type` tinyint(1) NOT NULL COMMENT '资源类型(0：菜单、1：按钮、2：权限资源)',
  `parent_id` varchar(50) NOT NULL COMMENT '父级id',
  `name_cn` varchar(50) NOT NULL COMMENT '中文名称',
  `name_en` varchar(50) DEFAULT NULL COMMENT '英文名称',
  `url` varchar(100) NOT NULL COMMENT '资源链接',
  `permission` varchar(100) NOT NULL COMMENT '权限字符串',
  `sort_no` int(10) DEFAULT NULL COMMENT '排序序号',
  `icon` varchar(100) DEFAULT NULL COMMENT '图标',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资源表';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `sys_resource`
--

LOCK TABLES `sys_resource` WRITE;
/*!40000 ALTER TABLE `sys_resource` DISABLE KEYS */;
INSERT INTO `sys_resource` VALUES ('5e0ea7d731c72ae05abae2b2',0,'0','系统管理',NULL,'/system/manager','system:manager',1,NULL,'2020-01-03 10:32:55','2020-01-03 10:32:55'),('5e0ea82b31c72ae05abae2b4',0,'5e0ea7d731c72ae05abae2b2','资源管理',NULL,'/resource/manager','resource:manager',1,NULL,'2020-01-03 10:34:19','2020-01-03 10:34:19'),('5e0ea86d31c72ae05abae2b6',1,'5e0ea82b31c72ae05abae2b4','新增资源',NULL,'/resource/insert','resource:insert',1,NULL,'2020-01-03 10:35:25','2020-01-03 10:35:25'),('5e0ea88931c72ae05abae2b8',1,'5e0ea82b31c72ae05abae2b4','删除资源',NULL,'/resource/delete','resource:delete',2,NULL,'2020-01-03 10:35:53','2020-01-03 10:35:53'),('5e0ea89931c72ae05abae2ba',1,'5e0ea82b31c72ae05abae2b4','更新资源',NULL,'/resource/update','resource:update',3,NULL,'2020-01-03 10:36:09','2020-01-03 10:36:09'),('5e0ea8be31c72ae05abae2bc',1,'5e0ea82b31c72ae05abae2b4','资源列表',NULL,'/resource/selectAll','resource:selectAll',4,NULL,'2020-01-03 10:36:46','2020-01-03 10:36:46'),('5e0ea90931c72ae05abae2be',0,'5e0ea7d731c72ae05abae2b2','用户管理',NULL,'/user/manager','user:manager',2,NULL,'2020-01-03 10:38:01','2020-01-03 10:38:01'),('5e0ea93031c72ae05abae2c0',1,'5e0ea90931c72ae05abae2be','新增用户',NULL,'/user/insert','user:insert',1,NULL,'2020-01-03 10:38:40','2020-01-03 10:38:40'),('5e0ea94931c72ae05abae2c2',1,'5e0ea90931c72ae05abae2be','删除用户',NULL,'/user/delete','user:delete',2,NULL,'2020-01-03 10:39:05','2020-01-03 10:39:05'),('5e0ea95831c72ae05abae2c4',1,'5e0ea90931c72ae05abae2be','用户列表',NULL,'/user/selectAll','user:selectAll',3,NULL,'2020-01-03 10:39:20','2020-01-03 10:39:20'),('5e0ea96631c72ae05abae2c6',1,'5e0ea90931c72ae05abae2be','更新用户',NULL,'/user/update','user:update',4,NULL,'2020-01-03 10:39:34','2020-01-03 10:39:34'),('5e0eadcd31c799d2d5ebf54a',0,'5e0ea7d731c72ae05abae2b2','角色管理',NULL,'/role/manager','role:manager',3,NULL,'2020-01-03 10:58:21','2020-01-03 10:58:21'),('5e0eadf131c799d2d5ebf54c',1,'5e0eadcd31c799d2d5ebf54a','新增角色',NULL,'/role/insert','role:insert',1,NULL,'2020-01-03 10:58:57','2020-01-03 10:58:57'),('5e0eadfc31c799d2d5ebf54e',1,'5e0eadcd31c799d2d5ebf54a','删除角色',NULL,'/role/delete','role:delete',2,NULL,'2020-01-03 10:59:08','2020-01-03 10:59:08'),('5e0eae2131c799d2d5ebf550',1,'5e0eadcd31c799d2d5ebf54a','角色列表',NULL,'/role/selectAll','role:selectAll',3,NULL,'2020-01-03 10:59:45','2020-01-03 10:59:45'),('5e0eae3031c799d2d5ebf552',1,'5e0eadcd31c799d2d5ebf54a','更新角色',NULL,'/role/update','role:update',4,NULL,'2020-01-03 11:00:00','2020-01-03 11:00:00'),('5e1ff82adfb1acd28dbd1b26',0,'5e0ea7d731c72ae05abae2b2','系统参数管理',NULL,'/config/manager','config:manager',1,NULL,'2020-01-16 13:44:10','2020-01-16 13:44:10'),('5e1ff86fdfb1d2b22362f487',1,'5e1ff82adfb1acd28dbd1b26','新增系统参数',NULL,'/config/insert','config:insert',1,NULL,'2020-01-16 13:45:19','2020-01-16 13:45:19'),('5e1ff88adfb166cdf0bd2e6f',1,'5e1ff82adfb1acd28dbd1b26','删除系统参数',NULL,'/config/delete','config:delete',2,NULL,'2020-01-16 13:45:46','2020-01-16 13:45:46'),('5e1ff8a2dfb18853315b208a',1,'5e1ff82adfb1acd28dbd1b26','系统参数列表',NULL,'/config/selectAll','config:selectAll',3,NULL,'2020-01-16 13:46:10','2020-01-16 13:46:10'),('5e1ff8b9dfb1c13471fe4a76',1,'5e1ff82adfb1acd28dbd1b26','修改系统参数',NULL,'/config/update','config:update',4,NULL,'2020-01-16 13:46:33','2020-01-16 13:46:33');
/*!40000 ALTER TABLE `sys_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role`
--

DROP TABLE IF EXISTS `sys_role`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `sys_role` (
  `id` varchar(50) NOT NULL COMMENT '角色id',
  `role_name` varchar(50) NOT NULL COMMENT '角色名称',
  `description` varchar(100) DEFAULT NULL COMMENT '角色描述',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `sys_role`
--

LOCK TABLES `sys_role` WRITE;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` VALUES ('5e0eae6e31c799d2d5ebf554','超级管理员','系统的超管','2020-01-03 11:01:02','2020-01-03 11:01:02'),('5e0eae8931c799d2d5ebf556','数据管理员','管理系统所有数据维护','2020-01-03 11:01:29','2020-01-03 11:01:29'),('5e0eaea831c799d2d5ebf558','技术人员','系统开发技术人员','2020-01-03 11:02:00','2020-01-03 11:02:00'),('5e0eaeb031c799d2d5ebf55a','测试人员','系统测试人员','2020-01-03 11:02:08','2020-01-03 11:02:08');
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_resource`
--

DROP TABLE IF EXISTS `sys_role_resource`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `sys_role_resource` (
  `role_id` varchar(50) NOT NULL COMMENT '角色id(对应sys_role表主键)',
  `resource_id` varchar(50) NOT NULL COMMENT '资源id(对应sys_resource表主键)',
  PRIMARY KEY (`role_id`,`resource_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色资源关联表';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `sys_role_resource`
--

LOCK TABLES `sys_role_resource` WRITE;
/*!40000 ALTER TABLE `sys_role_resource` DISABLE KEYS */;
INSERT INTO `sys_role_resource` VALUES ('5e0eae6e31c799d2d5ebf554','5e0ea7d731c72ae05abae2b2'),('5e0eae6e31c799d2d5ebf554','5e0ea82b31c72ae05abae2b4'),('5e0eae6e31c799d2d5ebf554','5e0ea86d31c72ae05abae2b6'),('5e0eae6e31c799d2d5ebf554','5e0ea88931c72ae05abae2b8'),('5e0eae6e31c799d2d5ebf554','5e0ea89931c72ae05abae2ba'),('5e0eae6e31c799d2d5ebf554','5e0ea8be31c72ae05abae2bc'),('5e0eae6e31c799d2d5ebf554','5e0ea90931c72ae05abae2be'),('5e0eae6e31c799d2d5ebf554','5e0ea93031c72ae05abae2c0'),('5e0eae6e31c799d2d5ebf554','5e0ea94931c72ae05abae2c2'),('5e0eae6e31c799d2d5ebf554','5e0ea95831c72ae05abae2c4'),('5e0eae6e31c799d2d5ebf554','5e0ea96631c72ae05abae2c6'),('5e0eae6e31c799d2d5ebf554','5e0eadcd31c799d2d5ebf54a'),('5e0eae6e31c799d2d5ebf554','5e0eadf131c799d2d5ebf54c'),('5e0eae6e31c799d2d5ebf554','5e0eadfc31c799d2d5ebf54e'),('5e0eae6e31c799d2d5ebf554','5e0eae2131c799d2d5ebf550'),('5e0eae6e31c799d2d5ebf554','5e0eae3031c799d2d5ebf552'),('5e0eae6e31c799d2d5ebf554','5e1ff82adfb1acd28dbd1b26'),('5e0eae6e31c799d2d5ebf554','5e1ff86fdfb1d2b22362f487'),('5e0eae6e31c799d2d5ebf554','5e1ff88adfb166cdf0bd2e6f'),('5e0eae6e31c799d2d5ebf554','5e1ff8a2dfb18853315b208a'),('5e0eae6e31c799d2d5ebf554','5e1ff8b9dfb1c13471fe4a76');
/*!40000 ALTER TABLE `sys_role_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `sys_user` (
  `id` varchar(50) NOT NULL COMMENT '主键ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_user_id` varchar(50) DEFAULT NULL COMMENT '更新人ID',
  `login_name` varchar(50) DEFAULT NULL COMMENT '登录账号',
  `login_pass` varchar(50) DEFAULT NULL COMMENT '登录密码',
  `user_name` varchar(10) DEFAULT NULL COMMENT '真实姓名',
  `mobile` varchar(12) DEFAULT NULL COMMENT '手机号',
  `state` int(1) DEFAULT NULL COMMENT '账号状态（1：正常；2：禁用；）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `login_name` (`login_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统用户表';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES ('1',NULL,NULL,NULL,'admin','c2549479bcce11b6af07294ae8fc8d2a','张三','18086295423',NULL),('5e0d8824dfb1cb9533ff69e6',NULL,NULL,NULL,'test001','c2549479bcce11b6af07294ae8fc8d2a','测试账号001','18086295411',NULL),('5e0d88f9dfb1e1756ba21b26',NULL,NULL,NULL,'test002','c2549479bcce11b6af07294ae8fc8d2a','测试账号002','18086295412',NULL),('5e0d88f9dfb1e1756ba21b27',NULL,NULL,NULL,'test003','c2549479bcce11b6af07294ae8fc8d2a','测试账号003','18086295413',NULL),('5e0d88f9dfb1e1756ba21b28',NULL,NULL,NULL,'test004','c2549479bcce11b6af07294ae8fc8d2a','测试账号004','18086295414',NULL),('5e0d88f9dfb1e1756ba21b29',NULL,NULL,NULL,'test005','c2549479bcce11b6af07294ae8fc8d2a','测试账号005','18086295415',NULL),('5e0d88f9dfb1e1756ba21b2a',NULL,NULL,NULL,'test006','c2549479bcce11b6af07294ae8fc8d2a','测试账号006','18086295416',NULL),('5e0d88f9dfb1e1756ba21b2b',NULL,NULL,NULL,'test007','c2549479bcce11b6af07294ae8fc8d2a','测试账号007','18086295417',NULL),('5e0d88f9dfb1e1756ba21b2c',NULL,NULL,NULL,'test008','c2549479bcce11b6af07294ae8fc8d2a','测试账号008','18086295418',NULL),('5e0d88f9dfb1e1756ba21b2d',NULL,NULL,NULL,'test009','c2549479bcce11b6af07294ae8fc8d2a','测试账号009','18086295419',NULL),('5e0d890edfb15b15d3048881',NULL,NULL,NULL,'test0010','c2549479bcce11b6af07294ae8fc8d2a','测试账号0010','18086295410',NULL),('5e0d890fdfb15b15d3048882',NULL,NULL,NULL,'test0011','c2549479bcce11b6af07294ae8fc8d2a','测试账号0011','18086295411',NULL),('5e0d890fdfb15b15d3048883',NULL,NULL,NULL,'test0012','c2549479bcce11b6af07294ae8fc8d2a','测试账号0012','18086295412',NULL),('5e0d890fdfb15b15d3048884',NULL,NULL,NULL,'test0013','c2549479bcce11b6af07294ae8fc8d2a','测试账号0013','18086295413',NULL),('5e0d890fdfb15b15d3048885',NULL,NULL,NULL,'test0014','c2549479bcce11b6af07294ae8fc8d2a','测试账号0014','18086295414',NULL),('5e0d890fdfb15b15d3048886',NULL,NULL,NULL,'test0015','c2549479bcce11b6af07294ae8fc8d2a','测试账号0015','18086295415',NULL),('5e0d890fdfb15b15d3048887',NULL,NULL,NULL,'test0016','c2549479bcce11b6af07294ae8fc8d2a','测试账号0016','18086295416',NULL),('5e0d890fdfb15b15d3048888',NULL,NULL,NULL,'test0017','c2549479bcce11b6af07294ae8fc8d2a','测试账号0017','18086295417',NULL),('5e0d890fdfb15b15d3048889',NULL,NULL,NULL,'test0018','c2549479bcce11b6af07294ae8fc8d2a','测试账号0018','18086295418',NULL),('5e0d890fdfb15b15d304888a',NULL,NULL,NULL,'test0019','c2549479bcce11b6af07294ae8fc8d2a','测试账号0019','18086295419',NULL),('5e0d890fdfb15b15d304888b',NULL,NULL,NULL,'test0020','c2549479bcce11b6af07294ae8fc8d2a','测试账号0020','18086295420',NULL),('5e0d890fdfb15b15d304888c',NULL,NULL,NULL,'test0021','c2549479bcce11b6af07294ae8fc8d2a','测试账号0021','18086295421',NULL),('5e0d890fdfb15b15d304888d',NULL,NULL,NULL,'test0022','c2549479bcce11b6af07294ae8fc8d2a','测试账号0022','18086295422',NULL),('5e0d890fdfb15b15d304888e',NULL,NULL,NULL,'test0023','c2549479bcce11b6af07294ae8fc8d2a','测试账号0023','18086295423',NULL),('5e0d890fdfb15b15d304888f',NULL,NULL,NULL,'test0024','c2549479bcce11b6af07294ae8fc8d2a','测试账号0024','18086295424',NULL),('5e0d890fdfb15b15d3048890',NULL,NULL,NULL,'test0025','c2549479bcce11b6af07294ae8fc8d2a','测试账号0025','18086295425',NULL),('5e0d890fdfb15b15d3048891',NULL,NULL,NULL,'test0026','c2549479bcce11b6af07294ae8fc8d2a','测试账号0026','18086295426',NULL),('5e0d890fdfb15b15d3048892',NULL,NULL,NULL,'test0027','c2549479bcce11b6af07294ae8fc8d2a','测试账号0027','18086295427',NULL),('5e0d890fdfb15b15d3048893',NULL,NULL,NULL,'test0028','c2549479bcce11b6af07294ae8fc8d2a','测试账号0028','18086295428',NULL),('5e0d890fdfb15b15d3048894',NULL,NULL,NULL,'test0029','c2549479bcce11b6af07294ae8fc8d2a','测试账号0029','18086295429',NULL),('5e0d890fdfb15b15d3048895',NULL,NULL,NULL,'test0030','c2549479bcce11b6af07294ae8fc8d2a','测试账号0030','18086295430',NULL),('5e0d890fdfb15b15d3048896',NULL,NULL,NULL,'test0031','c2549479bcce11b6af07294ae8fc8d2a','测试账号0031','18086295431',NULL),('5e0d890fdfb15b15d3048897',NULL,NULL,NULL,'test0032','c2549479bcce11b6af07294ae8fc8d2a','测试账号0032','18086295432',NULL),('5e0d890fdfb15b15d3048898',NULL,NULL,NULL,'test0033','c2549479bcce11b6af07294ae8fc8d2a','测试账号0033','18086295433',NULL),('5e0d890fdfb15b15d3048899',NULL,NULL,NULL,'test0034','c2549479bcce11b6af07294ae8fc8d2a','测试账号0034','18086295434',NULL),('5e0d890fdfb15b15d304889a',NULL,NULL,NULL,'test0035','c2549479bcce11b6af07294ae8fc8d2a','测试账号0035','18086295435',NULL),('5e0d890fdfb15b15d304889b',NULL,NULL,NULL,'test0036','c2549479bcce11b6af07294ae8fc8d2a','测试账号0036','18086295436',NULL),('5e0d890fdfb15b15d304889c',NULL,NULL,NULL,'test0037','c2549479bcce11b6af07294ae8fc8d2a','测试账号0037','18086295437',NULL),('5e0d890fdfb15b15d304889d',NULL,NULL,NULL,'test0038','c2549479bcce11b6af07294ae8fc8d2a','测试账号0038','18086295438',NULL),('5e0d890fdfb15b15d304889e',NULL,NULL,NULL,'test0039','c2549479bcce11b6af07294ae8fc8d2a','测试账号0039','18086295439',NULL),('5e0d890fdfb15b15d304889f',NULL,NULL,NULL,'test0040','c2549479bcce11b6af07294ae8fc8d2a','测试账号0040','18086295440',NULL),('5e0d890fdfb15b15d30488a0',NULL,NULL,NULL,'test0041','c2549479bcce11b6af07294ae8fc8d2a','测试账号0041','18086295441',NULL),('5e0d890fdfb15b15d30488a1',NULL,NULL,NULL,'test0042','c2549479bcce11b6af07294ae8fc8d2a','测试账号0042','18086295442',NULL),('5e0d890fdfb15b15d30488a2',NULL,NULL,NULL,'test0043','c2549479bcce11b6af07294ae8fc8d2a','测试账号0043','18086295443',NULL),('5e0d890fdfb15b15d30488a3',NULL,NULL,NULL,'test0044','c2549479bcce11b6af07294ae8fc8d2a','测试账号0044','18086295444',NULL),('5e0d890fdfb15b15d30488a4',NULL,NULL,NULL,'test0045','c2549479bcce11b6af07294ae8fc8d2a','测试账号0045','18086295445',NULL),('5e0d890fdfb15b15d30488a5',NULL,NULL,NULL,'test0046','c2549479bcce11b6af07294ae8fc8d2a','测试账号0046','18086295446',NULL),('5e0d890fdfb15b15d30488a6',NULL,NULL,NULL,'test0047','c2549479bcce11b6af07294ae8fc8d2a','测试账号0047','18086295447',NULL),('5e0d890fdfb15b15d30488a7',NULL,NULL,NULL,'test0048','c2549479bcce11b6af07294ae8fc8d2a','测试账号0048','18086295448',NULL),('5e0d890fdfb15b15d30488a8',NULL,NULL,NULL,'test0049','c2549479bcce11b6af07294ae8fc8d2a','测试账号0049','18086295449',NULL),('5e0d890fdfb15b15d30488a9',NULL,NULL,NULL,'test0050','c2549479bcce11b6af07294ae8fc8d2a','测试账号0050','18086295450',NULL),('5e0eb9d531c75513bff9b717',NULL,NULL,NULL,'test1001','c2549479bcce11b6af07294ae8fc8d2a','测试账号1001','18086290000',NULL);
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user_role`
--

DROP TABLE IF EXISTS `sys_user_role`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `sys_user_role` (
  `user_id` varchar(50) NOT NULL COMMENT '用户id(对应sys_user表主键)',
  `role_id` varchar(50) NOT NULL COMMENT '角色id(对应sys_role表主键)',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色关联表';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `sys_user_role`
--

LOCK TABLES `sys_user_role` WRITE;
/*!40000 ALTER TABLE `sys_user_role` DISABLE KEYS */;
INSERT INTO `sys_user_role` VALUES ('1','5e0eae6e31c799d2d5ebf554');
/*!40000 ALTER TABLE `sys_user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-19  7:06:48
