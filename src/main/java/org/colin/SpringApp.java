package org.colin;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author wujiangbo
 * @desc 项目启动类
 * @date 2020年1月2日 上午10:18:08
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class}) // - springboot项目,并配置扫描路径
@EnableTransactionManagement
@EnableCaching
public class SpringApp extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(SpringApp.class);
        // Banner.Mode.OFF 关闭
        springApplication.setBannerMode(Banner.Mode.CONSOLE);
        springApplication.run(args);
    }

    /**
     * 需要把web项目打成war包部署到外部tomcat运行时需要改变启动方式
     */
//    @Override
//    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
//        return builder.sources(SpringApp.class);
//    }
}
