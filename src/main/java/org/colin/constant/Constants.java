package org.colin.constant;

/**
 * @desc 常量类
 * @author wujiangbo
 * @date 2020年1月3日 上午11:17:32
 */
public class Constants {

	// 存放token的key
	public static final String TOKEN_KEY_STRING = "token:";

	// Redis中存储的用户对象信息字段的key值
	public static final String REDIS_USER_INFO_KEY = "userInfo:";

	// Redis中存储的用户的Map中用户信息的key
	public static final String MAP_USER_INFO_KEY = "userInfo";

	// Redis中存储的用户的Map中用户权限信息的key
	public static final String MAP_PERMISSION_LIST_KEY = "permissionList";

	// 系统编码
	public static final String SYSTEM_ENCODING = "UTF-8";

	// 登录图片验证码图片存Redis的key前缀
	public static final String PICTUREVERIFICATION = "pictureVerification:";

	// 成功返回编码
	public static final String SUCCESS_CODE = "000";

	// 失败返回编码
	public static final String FAIL_CODE = "001";

	// 账号状态（1：正常；2：禁用；）
	public static final String ACCOUNT_STATE_A = "1";

	public static final String ACCOUNT_STATE_B = "2";

}
