package org.colin.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.colin.model.ro.SysLogRO;
import org.colin.model.vo.SysLogVO;

@Mapper
public interface SysLogMapper {

	// 新增
	int insert(SysLogRO sysLogRO);

	// 删除
	int deleteByPrimaryKey(String logId);

	// 批量删除
	int deleteBatch(String[] ids);

	// 查询所有
	List<SysLogVO> selectAll(SysLogRO sysLogRO);

	// 查询所有大小
	int selectAllCount(SysLogRO sysLogRO);

	// 更新
	int update(SysLogRO sysLogRO);

}