package org.colin.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.colin.model.entity.SysUser;
import org.colin.model.ro.UserRoleRO;
import org.colin.model.vo.CustVO;
import org.colin.model.vo.UserVO;

import java.util.List;

@Mapper
public interface SysUserMapper {

    // 新增
    int insert(SysUser record);

    // 删除
    int deleteByPrimaryKey(String id);

    // 根据主键ID查询单个对象
    UserVO selectByPrimaryKey(String id);

    // 根据登录账号查询单个对象
    UserVO selectByLoginName(String loginName);

    // 查询对象集合
    List<UserVO> selectAll(SysUser user);

    // 查询对象集合
    List<CustVO> selectAllCust();

    // 查询所有用户
    List<UserVO> selectAllUser();

    // 查询对象集合数量
    int selectAllCount(SysUser user);

    // 更新对象
    int updateByPrimaryKeySelective(SysUser record);

    // 新增用户角色关联信息
    int addUserRole(UserRoleRO userRoleRO);

    // 更新用户角色关联信息
    int updateUserRole(UserRoleRO userRoleRO);

}
