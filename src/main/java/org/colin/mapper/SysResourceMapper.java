package org.colin.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.colin.model.ro.SysResourceRO;
import org.colin.model.vo.MenuVO;
import org.colin.model.vo.SysResourceVO;

@Mapper
public interface SysResourceMapper {

	// 新增
	int insert(SysResourceRO record);

	// 删除
	int deleteByPrimaryKey(String id);

	// 查询单个
	SysResourceVO selectByPrimaryKey(String id);

	// 查询所有资源
	List<SysResourceVO> selectAll(SysResourceRO record);

	// 更新
	int update(SysResourceRO record);

	// 根据角色ID查询所有资源信息
	List<SysResourceVO> selectResourceByRoleId(String roleId);

	// 根据用户ID查询拥有的所有权限字符串集合
	List<SysResourceVO> selectPermissionByUserId(String userId);

	// 查询用户拥有权限的所有菜单数据
	List<MenuVO> menuInit(String userId);

	// 根据id查询菜单
	MenuVO selectMenuById(String id);

}