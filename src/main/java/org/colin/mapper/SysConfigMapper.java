package org.colin.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.colin.model.ro.SysConfigRO;
import org.colin.model.vo.SysConfigVO;

@Mapper
public interface SysConfigMapper {

	int insert(SysConfigRO record);

	int deleteByPrimaryKey(String id);

	SysConfigVO selectByPrimaryKey(String id);

	List<SysConfigVO> selectAll();

	int updateByPrimaryKey(SysConfigRO record);
}