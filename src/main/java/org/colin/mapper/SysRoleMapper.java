package org.colin.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.colin.model.ro.RoleResourceRO;
import org.colin.model.ro.SysRoleRO;
import org.colin.model.vo.SysRoleVO;

@Mapper
public interface SysRoleMapper {

	// 新增
	int insert(SysRoleRO record);

	// 删除
	int deleteByPrimaryKey(String id);

	// 查询单个
	SysRoleVO selectByPrimaryKey(String id);

	// 根据用户ID查询角色信息
	List<SysRoleVO> selectRoleByUserId(String userId);

	// 查询所有
	List<SysRoleVO> selectAll(SysRoleRO record);

	// 更新
	int update(SysRoleRO record);

	// 添加角色权限资源
	int addRoleResource(RoleResourceRO roleResourceRO);

	// 删除角色所有的权限资源
	int deleteRoleResource(String roleId);
}