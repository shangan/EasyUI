package org.colin.ds;

import java.lang.annotation.*;
/**
 * @Description: 配置一个注解，方便使用，直接在需要配置的方法上面加上数据源即可
 * @ClassName: DataSource
 * @Author: wujiangbo
 * @Date: 2020-01-17 16:25
 * @Version: 1.1.0
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataSource {
    String name() default "";
}