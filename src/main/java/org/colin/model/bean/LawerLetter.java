package org.colin.model.bean;

import java.math.BigDecimal;
import lombok.Data;

/**
 * @desc 律师函模板中所需信息Bean
 * @author wujiangbo
 * @date 2020年1月4日 上午9:56:52
 */
@Data
public class LawerLetter {

	private String custName;// 客户姓名

	private String idNo;// 身份证号

	private String contractValidTime;// 贷款日期

	private BigDecimal loanAmount;// 贷款金额

	private String loanAmountString;// 贷款金额大写

	private String loanPeriod;// 偿还周期

	private String beginTimeCase;// 截止日期

	private BigDecimal yuqiAmount;// 本息总欠款

	private String currentDate;// 落款日期

	private String arbitrationDate;// 仲裁日期

	private String adjudicationDate;// 裁决日期

	private String caseNumber;// 案号

	private String contractNumber;// 合同编号

}
