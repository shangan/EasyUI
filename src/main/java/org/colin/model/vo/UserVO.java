package org.colin.model.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "用户数据模型")
public class UserVO implements Serializable {

	private static final long serialVersionUID = 8658684289134854700L;

	@ApiModelProperty(value = "主键ID(新增不传)", example = "", hidden = true)
	private String id;

	@ApiModelProperty(value = "创建时间", example = "", hidden = false)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;

	@ApiModelProperty(value = "更新时间", example = "", hidden = false)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime;

	@ApiModelProperty(value = "更新人ID", example = "", hidden = false)
	private String updateUserId;

	@ApiModelProperty(value = "更新人名称", example = "", hidden = false)
	private String updateUserName;

	@ApiModelProperty(value = "登录账号", example = "", hidden = false)
	private String loginName;

	@ApiModelProperty(value = "登录密码", example = "", hidden = false)
	private String loginPass;

	@ApiModelProperty(value = "姓名", example = "", hidden = false)
	private String userName;

	@ApiModelProperty(value = "手机号", example = "", hidden = false)
	private String mobile;

	@ApiModelProperty(value = "账号状态（1：正常；2：禁用；）", example = "", hidden = false)
	private Integer state;

	@ApiModelProperty(value = "角色集合")
	private List<SysRoleVO> roleList = new ArrayList<>();

}
