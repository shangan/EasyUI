package org.colin.model.vo;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 客户信息
 * @ClassName: CustVO
 * @Author: wujiangbo
 * @Date: 2020-01-17 17:21
 * @Version: 1.1.0
 */
@Data
@ApiModel(description = "客户信息-返回前端数据模型")
public class CustVO implements Serializable {

	private static final long serialVersionUID = 4755680881066602650L;

	@ApiModelProperty(value = "姓名", example = "", hidden = false)
	private String custName;

	@ApiModelProperty(value = "身份证号", example = "", hidden = false)
	private String custIdNumber;

	@ApiModelProperty(value = "年龄", example = "", hidden = false)
	private int age;

	@ApiModelProperty(value = "生日", example = "", hidden = false)
	private String birthday;

	@ApiModelProperty(value = "性别（0：男；1：女；2：保密）", example = "", hidden = false)
	private String custSex;

	@ApiModelProperty(value = "手机号", example = "", hidden = false)
	private String phoneNumber;

	@ApiModelProperty(value = "邮箱", example = "", hidden = false)
	private String email;

	@ApiModelProperty(value = "银行卡号", example = "", hidden = false)
	private String bankNumber;

	@ApiModelProperty(value = "银行名称", example = "", hidden = false)
	private String bankName;

	@ApiModelProperty(value = "户籍地址", example = "", hidden = false)
	private String registerAddress;

	@ApiModelProperty(value = "所属省份", example = "", hidden = false)
	private String province;
}