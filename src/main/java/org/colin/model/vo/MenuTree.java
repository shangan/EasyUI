package org.colin.model.vo;

import java.io.Serializable;
import java.util.List;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "EasyUI树形结构所需实体类")
public class MenuTree implements Serializable {

	private static final long serialVersionUID = 6004078914225965381L;

	@ApiModelProperty(value = "主键", example = "", hidden = false)
	private String id;

	@ApiModelProperty(value = "资源中文名称", example = "", hidden = false)
	private String text;

	@ApiModelProperty(value = "父节点ID", example = "", hidden = false)
	private String pId;

	@ApiModelProperty(value = "资源链接", example = "", hidden = false)
	private String url;

	@ApiModelProperty(value = "图标", example = "", hidden = false)
	private String iconCls;

	@ApiModelProperty(value = "状态（closed：合起；open：打开）", example = "", hidden = false)
	private String state;

	@ApiModelProperty(value = "序号", example = "", hidden = false)
	private Integer sort;

	private List<MenuTree> children;
}
