package org.colin.model.vo;

import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

@Data
public class SysLogVO implements Serializable {

	private static final long serialVersionUID = 6839368898041022579L;

	private String logId;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;// 记录时间

	private String type;// 日志类型（info/error）

	private String title;// 内容

	private String remoteAddr;// IP地址

	private String exception;// 异常信息

	private String userName;// 操作者
}
