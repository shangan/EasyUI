package org.colin.model.vo;

import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "系统角色-数据模型")
public class SysRoleVO implements Serializable {

	private static final long serialVersionUID = -732822258110798644L;

	@ApiModelProperty(value = "主键ID", example = "")
	private String id;

	@ApiModelProperty(value = "角色名称", example = "")
	private String roleName;

	@ApiModelProperty(value = "角色描述", example = "")
	private String description;

	@ApiModelProperty(value = "创建时间", example = "")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;

	@ApiModelProperty(value = "更新时间", example = "")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime;

}
