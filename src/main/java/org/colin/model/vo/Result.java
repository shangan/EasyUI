package org.colin.model.vo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.colin.enums.ResultCode;
import org.colin.model.ro.PageRO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @desc 接口统一返回数据模型
 * @author wujiangbo
 * @date 2020年1月2日 上午11:10:10
 */
@Data
@ApiModel(description = "返回结果集数据类型")
public class Result<T> implements Serializable {

	private static final long serialVersionUID = -8437975865926132058L;

	@ApiModelProperty(value = "结果状态码(000：成功；001：失败；其它编码：其它含义)")
	private String code;

	@ApiModelProperty(value = "返回提示消息")
	private String message;

	@ApiModelProperty(value = "返回数据")
	private Map<String, Object> data = new HashMap<>();

	private Result(List<T> list, int count, int pageSize) {
		this.code = "000";
		this.message = "成功";
		this.data.put("list", list);
		this.data.put("count", count);
		this.data.put("pageSize", pageSize);
	}

	private Result(List<T> list, int count, PageRO page) {
		this.code = "000";
		this.message = "成功";
		this.data.put("list", list);
		this.data.put("count", count);
		this.data.put("pageSize", page.getPageSize());
		this.data.put("currentPage", page.getCurrentPage());
	}

	private Result(List<T> list) {
		this.code = "000";
		this.message = "成功";
		this.data.put("list", list);
	}

	private Result(Map<String, Object> data) {
		this.code = "000";
		this.message = "成功";
		this.data.put("map", data);
	}

	private Result(T obj) {
		this.code = "000";
		this.message = "成功";
		this.data.put("vo", obj);
	}

	// 返回成功
	public Result<T> success(List<T> list, int count, int pageSize) {
		return new Result<T>(list, count, pageSize);
	}

	// 返回成功
	public static Result success(List list, int count, PageRO page) {
		return new Result(list, count, page);
	}

	// 返回成功
	public static Result success(List list) {
		return new Result(list);
	}

	// 返回成功
	public static Result success(Object obj) {
		return new Result(obj);
	}

	// 返回成功
	public static Result success(Map<String, Object> data) {
		return new Result(data);
	}

	// 返回成功
	public static Result success() {
		return new Result(ResultCode.SUCCESS);
	}

	// 返回失败
	public static Result fail() {
		return new Result(ResultCode.FAIL);
	}

	// 返回失败
	public static Result fail(ResultCode resultCode) {
		return new Result(resultCode);
	}

	// 返回失败
	public static Result fail(String code, String message) {
		return new Result(code, message);
	}

	private Result(ResultCode resultCode) {
		this.code = resultCode.code();
		this.message = resultCode.message();
	}

	private Result(String code, String message) {
		this.code = code;
		this.message = message;
	}

	private Result() {
	}

	@Override
	public String toString() {
		return "Result [code=" + code + ", message=" + message + ", data=" + data + "]";
	}

}
