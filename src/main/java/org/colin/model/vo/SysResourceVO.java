package org.colin.model.vo;

import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "系统资源-数据模型")
public class SysResourceVO implements Serializable {

	private static final long serialVersionUID = -3843220745267268688L;

	@ApiModelProperty(value = "主键ID", example = "")
	private String id;

	@ApiModelProperty(value = "资源类型(0：菜单；1：按钮；2：权限资源；)", example = "0")
	private Integer resourceType;

	@ApiModelProperty(value = "上级资源ID", example = "0")
	private String parentId;

	@ApiModelProperty(value = "中文名称", example = "")
	private String nameCn;

	@ApiModelProperty(value = "英文名称", example = "")
	private String nameEn;

	@ApiModelProperty(value = "资源链接", example = "")
	private String url;

	@ApiModelProperty(value = "权限字符串", example = "")
	private String permission;

	@ApiModelProperty(value = "排序序号", example = "")
	private Integer sortNo;

	@ApiModelProperty(value = "图标", example = "")
	private String icon;

	@ApiModelProperty(value = "创建时间", example = "")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;

	@ApiModelProperty(value = "更新时间", example = "")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime;

}
