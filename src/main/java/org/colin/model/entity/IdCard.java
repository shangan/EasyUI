package org.colin.model.entity;

import lombok.Data;

/**
 * @desc 身份证实体类
 * @author wujiangbo
 * @date 2020年1月2日 下午6:03:41
 */
@Data
public class IdCard {
	// 身份证号码
	private String idCard;

	// 出生日期
	private String born;

	// 性别
	private String sex;

	// 所在地区
	private String att;

	// 国家-省-市
	private String styleCitynm;

}
