package org.colin.model.entity;

import java.util.Date;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.colin.model.ro.PageRO;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "系统用户数据模型")
public class SysUser extends PageRO {

	private static final long serialVersionUID = -3318439666411983711L;

	@ApiModelProperty(value = "主键ID(新增不传)", example = "", hidden = true)
	private String id;

	@ApiModelProperty(value = "创建时间", example = "", hidden = false)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;

	@ApiModelProperty(value = "更新时间", example = "", hidden = false)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime;

	@ApiModelProperty(value = "更新人ID", example = "", hidden = false)
	private String updateUserId;

	@ApiModelProperty(value = "登录账号", example = "", hidden = false)
	@NotBlank(message = "登录账号不能为空")
	private String loginName;

	@ApiModelProperty(value = "登录密码", example = "", hidden = false)
	@NotBlank(message = "登录密码不能为空")
	private String loginPass;

	@ApiModelProperty(value = "姓名", example = "", hidden = false)
	private String userName;

	@ApiModelProperty(value = "手机号", example = "", hidden = false)
	@Pattern(regexp = "^[1][3,4,5,6,7,8,9][0-9]{9}$", message = "手机号格式有误")
	@Max(value = 11, message = "手机号只能为{max}位")
	@Min(value = 11, message = "手机号只能为{min}位")
	@NotNull(message = "手机号不能为空")
	@NotBlank(message = "手机号不能为空")
	private String mobile;

	@ApiModelProperty(value = "账号状态（1：正常；2：禁用；）", example = "", hidden = false)
	private Integer state;

	@ApiModelProperty(value = "角色ID", example = "", hidden = false)
	private String roleId;

}
