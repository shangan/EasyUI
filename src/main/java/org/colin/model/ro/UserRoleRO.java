package org.colin.model.ro;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "用户角色数据模型")
public class UserRoleRO implements Serializable {

	private static final long serialVersionUID = 6018141037168021691L;

	@ApiModelProperty(value = "用户ID", example = "")
	private String userId;

	@ApiModelProperty(value = "角色ID", example = "")
	private String roleId;

}
