package org.colin.model.ro;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "登录数据模型")
public class LoginRO implements Serializable {

	private static final long serialVersionUID = -6364851049478283736L;

	@ApiModelProperty(value = "登录账号", example = "")
	@NotBlank(message = "登录账号不能为空")
	private String loginName;

	@NotBlank(message = "登录密码不能为空")
	@ApiModelProperty(value = "登录密码", example = "")
	private String loginPass;

	@NotBlank(message = "登录图片验证码不能为空")
	@ApiModelProperty(value = "登录图片验证码", example = "")
	private String captcha;
}
