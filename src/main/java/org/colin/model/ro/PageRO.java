package org.colin.model.ro;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @desc 分页查询参数模型
 * @author wujiangbo
 * @date 2020年1月2日 上午11:26:57
 */
@ApiModel(description = "分页查询参数模型")
public class PageRO implements Serializable {

	private static final long serialVersionUID = -9059212827557250412L;

	@ApiModelProperty(value = "当前页", example = "1")
	private int currentPage = 1;

	@ApiModelProperty(value = "每页条数(默认15条)", example = "15")
	private int pageSize = 15;

	@ApiModelProperty(value = "起始页数", example = "3")
	private int limitFrom;

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage <= 0 ? 1 : currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		if (pageSize > 0) {
			this.pageSize = pageSize;
		}
	}

	public int getLimitFrom() {
		limitFrom = (currentPage - 1) * pageSize;
		return limitFrom;
	}
}
