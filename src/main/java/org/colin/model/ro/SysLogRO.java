package org.colin.model.ro;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc 系统操作日志
 * @author wujiangbo
 * @date 2020年1月2日 下午2:59:19
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysLogRO extends PageRO {

	private static final long serialVersionUID = 8542174533369165439L;

	@ApiModelProperty(hidden = true)
	private String logId;

	@ApiModelProperty(hidden = true)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;

	@ApiModelProperty(value = "类型", example = "info/error")
	private String type;

	@ApiModelProperty(value = "内容", example = "")
	private String title;

	@ApiModelProperty(hidden = true)
	private String remoteAddr;

	@ApiModelProperty(hidden = true)
	private String requestUri;

	@ApiModelProperty(hidden = true)
	private String method;

	@ApiModelProperty(hidden = true)
	private String exception;

	@ApiModelProperty(hidden = true)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date operateDate;

	@ApiModelProperty(hidden = true)
	private String timeout;

	@ApiModelProperty(value = "操作者", example = "")
	private String userName;

	@ApiModelProperty(value = "用户ID", hidden = true)
	private String userId;

}
