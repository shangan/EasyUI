package org.colin.model.ro;

import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "系统配置-入参模型")
@Data
public class SysConfigRO implements Serializable {

	private static final long serialVersionUID = 7978280805129335361L;

	@ApiModelProperty(value = "主键ID", example = "", hidden = true)
	private String id;

	@ApiModelProperty(value = "创建时间", example = "", hidden = true)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;

	@ApiModelProperty(value = "更新时间", example = "", hidden = true)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime;

	@ApiModelProperty(value = "更新人ID", example = "", hidden = false)
	private String updateUserId;

	@ApiModelProperty(value = "key", example = "", hidden = false)
	private String keyStr;

	@ApiModelProperty(value = "value", example = "", hidden = false)
	private String valueStr;

	@ApiModelProperty(value = "配置描述", example = "", hidden = false)
	private String description;

	@ApiModelProperty(value = "类型", example = "", hidden = false)
	private Integer type;

	@ApiModelProperty(value = "标签", example = "", hidden = false)
	private String label;

	@ApiModelProperty(value = "排序", example = "", hidden = false)
	private Integer sort;

}