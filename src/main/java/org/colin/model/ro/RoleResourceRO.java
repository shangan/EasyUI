package org.colin.model.ro;

import java.io.Serializable;
import java.util.List;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "角色资源-请求参数模型")
public class RoleResourceRO implements Serializable {

	private static final long serialVersionUID = 503326018598677121L;

	@ApiModelProperty(value = "角色ID", example = "")
	private String roleId;

	@ApiModelProperty(value = "资源ID集合(所有选中的资源的ID集合)", example = "")
	private List<String> resourceIds;

}
