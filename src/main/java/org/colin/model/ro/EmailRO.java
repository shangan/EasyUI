package org.colin.model.ro;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * @Description: 发送邮件的JavaBean
 * @ClassName: EmailRO
 * @Author: wujiangbo
 * @Date: 2020-01-06 17:59
 * @Version: 1.1.0
 */
@Data
public class EmailRO implements Serializable {

    @ApiModelProperty(value = "接收者邮箱", example = "", hidden = false)
    private String to;

    @ApiModelProperty(value = "主题", example = "", hidden = false)
    private String object;

    @ApiModelProperty(value = "邮件内容", example = "", hidden = false)
    private String content;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("EmailRO{");
        sb.append("to='").append(to).append('\'');
        sb.append(", object='").append(object).append('\'');
        sb.append(", content='").append(content).append('\'');
        sb.append('}');
        return sb.toString();
    }
}