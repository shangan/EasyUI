package org.colin.tasks;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import lombok.extern.slf4j.Slf4j;

/**
 * @desc 定时任务
 * @author wujiangbo
 * @date 2020年1月3日 下午5:31:10
 */
@Component
@EnableScheduling
@Slf4j
public class Task {

	// 第一次延迟5秒后执行，之后按fixedRate的规则每x分钟执行一次(fixedRate = 1000 * 60 * 60 * 2 每2小时执行一次)
	@Scheduled(initialDelay = 5000, fixedRate = 1000 * 60 * 30)
	public void taskTest1() {
		log.info("--------定时任务--------每30分钟执行一次");
	}

}
