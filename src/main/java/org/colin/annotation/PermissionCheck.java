package org.colin.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @desc 权限检查注解（用于Controller接口的方法上，表示该接口需要进行权限验证）
 * @author wujiangbo
 * @date 2020年1月3日 下午3:12:12
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface PermissionCheck {

	/**
	 * @desc 权限字符串(为空则不校验)
	 * @author wujiangbo
	 * @date 2020年1月3日 下午3:12:38
	 * @return
	 */
	String permission() default "";
}
