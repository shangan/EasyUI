package org.colin.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @desc 自定义注解类-AOP实现后台操作日志记录
 * @author wujiangbo
 * @date 2020年1月2日 下午2:45:02
 */
@Target({ ElementType.PARAMETER, ElementType.METHOD }) // ElementType.PARAMETER:用于描述参数;ElementType.METHOD:用于描述方法
@Retention(RetentionPolicy.RUNTIME) // RetentionPolicy.RUNTIME:运行时有效
@Documented // @Documented用于描述其它类型的annotation应该被作为被标注的程序成员的公共API，因此可以被例如javadoc此类的工具文档化。Documented是一个标记注解，没有成员。
public @interface SystemControllerLog {

	/**
	 * @desc 描述业务操作（例：xxx管理--执行xxx操作）
	 * @author wujiangbo
	 * @date 2020年1月2日 下午2:45:46
	 * @return
	 */
	String description() default "";
}
