package org.colin.controller.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.hutool.http.useragent.Browser;
import cn.hutool.http.useragent.UserAgent;
import org.colin.Tools.MD5Util;
import org.colin.Tools.Tool;
import org.colin.annotation.PermissionCheck;
import org.colin.annotation.SystemControllerLog;
import org.colin.captcha.ArithmeticCaptcha;
import org.colin.constant.Constants;
import org.colin.controller.base.BaseController;
import org.colin.model.ro.LoginRO;
import org.colin.model.vo.Result;
import org.colin.model.vo.UserVO;
import org.colin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import java.net.InetAddress;

@Controller
@RequestMapping(value = "/")
@Api(tags = "系统登录注销-相关接口")
public class LoginController extends BaseController {

    @Autowired
    private ArithmeticCaptcha captcha;

    @Autowired
    private UserService userService;

    /**
     * 登录界面的显示
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {
        model.addAttribute("content", "Freemarker");
        return "login/login";
    }

    @GetMapping({"/loginSuccess"})
    public String toLoginSuccess() {
        return "main";
    }

    @ApiOperation(value = "获取图片验证码", notes = "获取图片验证码")
    @GetMapping("getPictureVerification")
    public void getPictureVerification(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        captcha.createImage(request, response, session);
    }

    @RequestMapping(value = "/goLogin", method = RequestMethod.POST)
    @ApiOperation(value = "系统登录", notes = "")
    public String login(HttpSession session, HttpServletRequest req, Model model) {
        String userName = req.getParameter("userName").trim();
        String password = req.getParameter("passWord");
        String captcha = req.getParameter("code");
        String sesionCode = (String) req.getSession().getAttribute(Constants.PICTUREVERIFICATION);
        if (Tool.isBlank(sesionCode)) {
            model.addAttribute("errormess", "图片验证码已过期");
            return "login/login";
        } else if (!sesionCode.equalsIgnoreCase(captcha)) {
            model.addAttribute("errormess", "验证码错误");
            return "login/login";
        }
        LoginRO loginRO = new LoginRO();
        loginRO.setLoginName(userName);
        loginRO.setLoginPass(password);
        loginRO.setCaptcha(captcha);
        UserVO user = userService.login(loginRO);
        if (user == null) {
            model.addAttribute("errormess", "该登录账号不存在");
            return "login/login";
        } else {
            // 匹配登录密码
            if (!user.getLoginPass().equals(MD5Util.md5(password))) {
                model.addAttribute("errormess", "登录密码错误");
                return "login/login";
            } else {
                //登录成功
                session.setAttribute(user.getId(), user);
            }
        }
        return "redirect:/index";
    }

    @RequestMapping(value = "/loginOut", method = RequestMethod.POST)
    @ResponseBody
    @SystemControllerLog(description = "系统注销")
    @ApiOperation(value = "系统注销", notes = "")
    @PermissionCheck(permission = "")
    public Result loginOut() {
        return userService.loginOut();
    }

    @RequestMapping(value = "/forceLoginOut", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "强制用户下线", notes = "")
    @PermissionCheck(permission = "")
    @ApiImplicitParams({@ApiImplicitParam(name = "userId", value = "用户ID", required = true, paramType = "query", dataType = "String")})
    public Result forceLoginOut(@RequestParam String userId) {
        return userService.forceLoginOut(userId);
    }
}
