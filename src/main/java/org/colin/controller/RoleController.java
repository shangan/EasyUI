package org.colin.controller;

import org.colin.annotation.PermissionCheck;
import org.colin.annotation.SystemControllerLog;
import org.colin.model.ro.RoleResourceRO;
import org.colin.model.ro.SysRoleRO;
import org.colin.model.vo.Result;
import org.colin.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping(value = "/role", produces = "application/json;charset=UTF-8")
@Api(tags = "角色管理-相关接口")
public class RoleController {

	@Autowired
	private RoleService roleService;

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ResponseBody
	@PermissionCheck(permission = "role:insert")
	@SystemControllerLog(description = "角色管理-新增角色")
	@ApiOperation(value = "新增角色", notes = "")
	public Result insert(@RequestBody SysRoleRO sysRoleRO) {
		return roleService.insert(sysRoleRO);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@PermissionCheck(permission = "role:delete")
	@SystemControllerLog(description = "角色管理-删除角色")
	@ApiOperation(value = "删除角色", notes = "")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "主键ID", required = true, paramType = "query", dataType = "String") })
	public Result delete(@RequestParam String id) {
		return roleService.delete(id);
	}

	@RequestMapping(value = "/selectAll", method = RequestMethod.POST)
	@ResponseBody
	@PermissionCheck(permission = "role:selectAll")
	@ApiOperation(value = "查询所有角色", notes = "")
	public Result selectAll(@RequestBody SysRoleRO sysRoleRO) {
		return roleService.selectAll(sysRoleRO);
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@PermissionCheck(permission = "role:update")
	@SystemControllerLog(description = "角色管理-更新角色")
	@ApiOperation(value = "更新角色", notes = "")
	public Result update(@RequestBody SysRoleRO sysRoleRO) {
		return roleService.update(sysRoleRO);
	}

	@SystemControllerLog(description = "角色管理-修改角色权限")
	@ApiOperation(value = "修改角色权限", notes = "")
	@RequestMapping(value = "/updateRoleResource", method = RequestMethod.POST)
	@ResponseBody
	@PermissionCheck(permission = "role:updateRoleResource")
	public Result updateRoleResource(@RequestBody RoleResourceRO roleResourceRO) {
		return roleService.updateRoleResource(roleResourceRO);
	}

	@ApiOperation(value = "根据角色ID查询所有资源信息", notes = "")
	@RequestMapping(value = "/selectResourceListByRoleId", method = RequestMethod.POST)
	@ApiImplicitParams({ @ApiImplicitParam(name = "roleId", value = "角色ID", required = true, paramType = "query", dataType = "String") })
	@PermissionCheck(permission = "")
	@ResponseBody
	public Result selectResourceListByRoleId(@RequestParam(name = "roleId") String roleId) {
		return roleService.selectResourceListByRoleId(roleId);
	}

}
