package org.colin.controller;

import javax.servlet.http.HttpServletResponse;
import org.colin.annotation.PermissionCheck;
import org.colin.annotation.SystemControllerLog;
import org.colin.model.entity.SysUser;
import org.colin.model.vo.Result;
import org.colin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * @author wujiangbo
 * @desc 处理用户相关请求
 * @date 2020年1月2日 下午12:04:57
 */
@Controller
@RequestMapping(value = "/user", produces = "application/json;charset=UTF-8")
@Api(tags = "用户管理相关接口")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ResponseBody
	@SystemControllerLog(description = "用户管理-新增用户")
	@ApiOperation(value = "新增系统用户", notes = "")
	@PermissionCheck(permission = "user:insert")
	public Result insert(@RequestBody @Validated SysUser user) {
		return userService.insert(user);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@SystemControllerLog(description = "用户管理-删除用户")
	@ApiOperation(value = "删除系统用户", notes = "")
	@PermissionCheck(permission = "user:delete")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "主键ID", required = true, paramType = "query", dataType = "String") })
	public Result delete(@RequestParam(required = true) String id) {
		return userService.delete(id);
	}

	@RequestMapping(value = "/selectAll", method = RequestMethod.POST)
	@ResponseBody
	@PermissionCheck(permission = "user:selectAll")
	@ApiOperation(value = "查询所有系统用户", notes = "")
	public Result selectAll(@RequestBody SysUser user) {
		return userService.selectAll(user);
	}

	@RequestMapping(value = "/selectAllCust", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(value = "查询所有客户", notes = "")
	public void selectAllCust(HttpServletResponse httpServletResponse) {
		userService.selectAllCust(httpServletResponse);
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@SystemControllerLog(description = "用户管理-更新用户")
	@PermissionCheck(permission = "user:update")
	@ApiOperation(value = "更新系统用户", notes = "")
	public Result update(@RequestBody SysUser user) {
		return userService.update(user);
	}
}
