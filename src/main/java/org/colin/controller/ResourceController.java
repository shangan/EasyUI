package org.colin.controller;

import org.colin.annotation.PermissionCheck;
import org.colin.annotation.SystemControllerLog;
import org.colin.model.ro.SysResourceRO;
import org.colin.model.vo.Result;
import org.colin.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * @author wujiangbo
 * @desc 资源管理
 * @date 2020年1月3日 上午10:02:10
 */
@Controller
@RequestMapping(value = "/resource", produces = "application/json;charset=UTF-8")
@Api(tags = "资源管理相关接口")
public class ResourceController {

	@GetMapping("/manager")
	public String login() {
		return "/resource/manager";
	}

	@Autowired
	private ResourceService resourceService;

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ResponseBody
	@PermissionCheck(permission = "resource:insert")
	@SystemControllerLog(description = "资源管理-新增资源")
	@ApiOperation(value = "新增资源", notes = "")
	public Result insert(@RequestBody SysResourceRO sysResourceRO) {
		return resourceService.insert(sysResourceRO);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@PermissionCheck(permission = "resource:delete")
	@SystemControllerLog(description = "资源管理-删除资源")
	@ApiOperation(value = "删除资源", notes = "")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "主键ID", required = true, paramType = "query", dataType = "String") })
	public Result delete(@RequestParam String id) {
		return resourceService.delete(id);
	}

	@RequestMapping(value = "/selectAll", method = RequestMethod.POST)
	@ResponseBody
	@PermissionCheck(permission = "resource:selectAll")
	@ApiOperation(value = "查询所有资源", notes = "")
	public Result selectAll(@RequestBody SysResourceRO sysResourceRO) {
		return resourceService.selectAll(sysResourceRO);
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@PermissionCheck(permission = "resource:update")
	@SystemControllerLog(description = "资源管理-更新资源")
	@ApiOperation(value = "更新资源", notes = "")
	public Result update(@RequestBody SysResourceRO sysResourceRO) {
		return resourceService.update(sysResourceRO);
	}

	@RequestMapping(value = "/menuInit", method = RequestMethod.POST)
	@ResponseBody
	@PermissionCheck(permission = "")
	@ApiOperation(value = "菜单初始化", notes = "登录系统后，初始化左侧菜单")
	public Result menuInit() {
		return resourceService.menuInit();
	}

	@RequestMapping(value = "/menuInitEasyUI", method = RequestMethod.POST)
	@ResponseBody
	@PermissionCheck(permission = "")
	@ApiOperation(value = "菜单初始化EasyUI", notes = "登录系统后，初始化左侧菜单")
	public Result menuInitEasyUI() {
		return resourceService.menuInitEasyUI();
	}

}
