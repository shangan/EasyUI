package org.colin.controller;

import org.colin.annotation.PermissionCheck;
import org.colin.annotation.SystemControllerLog;
import org.colin.model.ro.SysConfigRO;
import org.colin.model.vo.Result;
import org.colin.service.SysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping(value = "/config", produces = "application/json;charset=UTF-8")
@Api(tags = "系统参数配置-相关接口")
public class ConfigController {

	@Autowired
	private SysConfigService sysConfigService;

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ResponseBody
	@PermissionCheck(permission = "config:insert")
	@SystemControllerLog(description = "系统参数配置-新增")
	@ApiOperation(value = "新增系统配置参数", notes = "")
	public Result insert(@RequestBody SysConfigRO sysConfigRO) {
		return sysConfigService.insert(sysConfigRO);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@PermissionCheck(permission = "config:delete")
	@SystemControllerLog(description = "系统参数配置-删除")
	@ApiOperation(value = "删除系统配置参数", notes = "")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "主键ID", required = true, paramType = "query", dataType = "String") })
	public Result delete(@RequestParam String id) {
		return sysConfigService.delete(id);
	}

	@RequestMapping(value = "/selectAll", method = RequestMethod.POST)
	@ResponseBody
	@PermissionCheck(permission = "config:selectAll")
	@ApiOperation(value = "查询所有系统配置参数", notes = "")
	public Result selectAll() {
		return sysConfigService.selectAll();
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	@PermissionCheck(permission = "config:update")
	@SystemControllerLog(description = "角色管理-更新角色")
	@ApiOperation(value = "更新系统配置参数", notes = "")
	public Result update(@RequestBody SysConfigRO sysConfigRO) {
		return sysConfigService.update(sysConfigRO);
	}

}
