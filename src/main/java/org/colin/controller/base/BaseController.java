package org.colin.controller.base;

/**
 * @desc 抽象控制器
 * @author wujiangbo
 * @date 2020年1月18日 下午2:24:23
 */
public abstract class BaseController {

	/**
	 * @desc redirect跳转
	 * @author wujiangbo
	 * @date 2020年1月18日 下午2:24:05
	 * @param url 目标url
	 * @return 跳转地址
	 */
	public String redirect(String url) {
		return "redirect:".concat(url);
	}

}
