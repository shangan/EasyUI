package org.colin.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.colin.Tools.FileUtil;
import org.colin.Tools.QRCodeUtil;
import org.colin.Tools.RedisUtil;
import org.colin.annotation.Log;
import org.colin.annotation.PermissionCheck;
import org.colin.mapper.SysUserMapper;
import org.colin.model.ro.EmailRO;
import org.colin.model.vo.Result;
import org.colin.model.vo.UserVO;
import org.colin.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;

/**
 * @author wujiangbo
 * @desc 测试Controller
 * @date 2020年1月2日 下午12:05:11
 */
@Controller
@RequestMapping(value = "/test", produces = "application/json;charset=UTF-8")
@Slf4j
public class TestController {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private MailService mailService;

    @Autowired
    private FileUtil fileUtil;

    @Autowired
    private QRCodeUtil qRCodeUtil;

    @ApiOperation(value = "生成二维码", notes = "")
    @Log
    @PostMapping(value = "/qRCodeCreate")
    @ResponseBody
    public Result qRCodeCreate(@RequestParam String content) {
//        boolean flag = qRCodeUtil.zxingCodeCreate(content, "D:/123/", 500, "D:/123/5.jpg");
        boolean flag = qRCodeUtil.zxingCodeCreate(new File("D:/123/325.jpg"), new File("D:/测试.png"), "D:/123/11111.jpg");
//        com.google.zxing.Result result = qRCodeUtil.zxingCodeAnalyze("D:/123/733.jpg");
//        log.info("result={}",result);
        return Result.success();
    }

    @ApiOperation(value = "发送邮件", notes = "")
    @Log
    @PostMapping(value = "/sendEmail")
    @ResponseBody
    public Result sendEmail(@RequestBody EmailRO emailRO) {
        mailService.sendAttachmentMail(emailRO, "D:/律师函模板-普通案件.docx");
        return Result.success();
    }

    @RequestMapping(value = "/deleteFile", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "删除文件", notes = "")
    @Log
    public Result deleteFile(@RequestParam String filePath) {
        boolean flag = fileUtil.deleteOssFile(filePath);
        return Result.success(flag);
    }

    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "上传文件", notes = "")
    @Log
    public Result uploadFile(@RequestParam(value = "file") MultipartFile file) {
        String returnFilePath = fileUtil.uploadMultipartFile2Oss(file, "se/UploadRoot/testFile/");
        return Result.success(returnFilePath);
    }

    @RequestMapping(value = "/wjbTest", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "测试方法", notes = "")
    @PermissionCheck(permission = "user:insert")
    public Result wjbTest() {
        log.info("进入wjbTest方法");
        return Result.success();
    }

    @RequestMapping(value = "/getAllUser", method = RequestMethod.POST)
    @ResponseBody
    @Log
    @ApiOperation(value = "获取所有用户", notes = "")
    @PermissionCheck(permission = "user:insert")
    public Result getAllUser(@RequestParam String name) {
        List<UserVO> userList = sysUserMapper.selectAllUser();
        return Result.success(userList);
    }

    @RequestMapping(value = "/redisSet", method = RequestMethod.POST)
    @ResponseBody
    @PermissionCheck(permission = "")
    @ApiOperation(value = "往Redis添加值", notes = "")
    public Result redisSet(@RequestParam String key, @RequestParam String value) {
        redisUtil.set(key, value, 1);
        return Result.success();
    }

    @RequestMapping(value = "/redisGet", method = RequestMethod.POST)
    @ResponseBody
    @PermissionCheck(permission = "")
    @ApiOperation(value = "从Redis获取值", notes = "")
    public Result redisGet(@RequestParam String key) {
        String value = (String) redisUtil.get(key);
        return Result.success(value);
    }

    @RequestMapping(value = "/pageHelperTest", method = RequestMethod.POST)
    @ResponseBody
    @PermissionCheck(permission = "")
    @ApiOperation(value = "pageHelper分页工具类测试", notes = "")
    public Result pageHelperTest(@RequestParam(defaultValue = "1", value = "pageNum") Integer pageNum) {
        log.info("进入pageHelperTest方法");
        PageHelper.startPage(pageNum, 5);
        List<UserVO> userList = sysUserMapper.selectAllUser();
        PageInfo<UserVO> pageInfo = new PageInfo<UserVO>(userList);
        return Result.success(pageInfo);
    }
}
