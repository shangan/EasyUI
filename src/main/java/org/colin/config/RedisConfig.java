//package org.colin.config;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.redis.connection.RedisConnectionFactory;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
//import org.springframework.data.redis.serializer.RedisSerializer;
//import org.springframework.data.redis.serializer.StringRedisSerializer;
//
//@Configuration
//public class RedisConfig {
//
//	@Autowired
//	private RedisTemplate redisTemplate;
//
//	/**
//	 * @desc 解决Redis中乱码问题
//	 * @author wujiangbo
//	 * @date 2020年1月3日 下午4:56:23
//	 * @return
//	 */
//	@Bean
//	public RedisTemplate<String, Object> stringSerializerRedisTemplate() {
//		RedisSerializer<String> stringSerializer = new StringRedisSerializer();
//		redisTemplate.setKeySerializer(stringSerializer);
//		redisTemplate.setValueSerializer(stringSerializer);
//		redisTemplate.setHashKeySerializer(stringSerializer);
//		redisTemplate.setHashValueSerializer(stringSerializer);
//		return redisTemplate;
//	}
//
//	/**
//	 * @desc 实例化 RedisTemplate 对象
//	 * @author wujiangbo
//	 * @date 2020年1月3日 下午4:13:16
//	 * @param redisConnectionFactory
//	 * @return
//	 */
//	@Bean
//	public RedisTemplate<String, Object> functionDomainRedisTemplate(RedisConnectionFactory redisConnectionFactory) {
//		RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
//		initDomainRedisTemplate(redisTemplate, redisConnectionFactory);
//		return redisTemplate;
//	}
//
//	/**
//	 * @desc 设置数据存入 redis 的序列化方式,并开启事务
//	 * @author wujiangbo
//	 * @date 2020年1月3日 下午4:13:30
//	 * @param redisTemplate
//	 * @param factory
//	 */
//	private void initDomainRedisTemplate(RedisTemplate<String, Object> redisTemplate, RedisConnectionFactory factory) {
//		// 如果不配置Serializer，那么存储的时候缺省使用String
//		// 如果用User类型存储，那么会提示错误User can't cast to String！
//		redisTemplate.setKeySerializer(new StringRedisSerializer());
//		redisTemplate.setHashKeySerializer(new StringRedisSerializer());
//		redisTemplate.setHashValueSerializer(new GenericJackson2JsonRedisSerializer());
//		redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer());
//		// 开启事务
//		redisTemplate.setEnableTransactionSupport(true);
//		redisTemplate.setConnectionFactory(factory);
//	}
//}
