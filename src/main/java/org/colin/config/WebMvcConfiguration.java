package org.colin.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * @desc 文件映射
 * @author wujiangbo
 * @date 2020年1月3日 下午5:32:36
 */
@Configuration
public class WebMvcConfiguration extends WebMvcConfigurationSupport {

	@Value("${system.fileMapping}")
	private String localFilePath;

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/FileResource/**").addResourceLocations("file:" + localFilePath);
		registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
		registry.addResourceHandler("/templates/**").addResourceLocations("classpath:/templates/");
		super.addResourceHandlers(registry);
	}
}
