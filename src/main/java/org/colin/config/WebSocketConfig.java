package org.colin.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * @desc WebSocket配置类
 * @author wujiangbo
 * @date 2020年1月16日 下午2:45:23
 */
@Configuration
public class WebSocketConfig {

	/**
	 * 用war包跑要用websocket就注释，用jar包跑需要放开
	 */
	@Bean
	public ServerEndpointExporter serverEndpointExporter() {
		return new ServerEndpointExporter();
	}
}
