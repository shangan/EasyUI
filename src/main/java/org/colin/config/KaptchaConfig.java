package org.colin.config;

import java.util.Properties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;

/**
 * @desc 生成验证码配置
 * @author wujiangbo
 * @date 2020年1月18日 上午10:01:59
 */
@Configuration
public class KaptchaConfig {

	@Bean
	public DefaultKaptcha producer() {
		Properties properties = new Properties();
		properties.put("kaptcha.border", "no");// 设置是否有边框（yes/no）
		properties.put("kaptcha.textproducer.font.color", "black");// 设置字体颜色（black）
		properties.put("kaptcha.textproducer.char.space", "3");// 字符间距
		properties.put("kaptcha.image.height", "26");// 设置验证码高度
		properties.put("kaptcha.image.width", "70");// 设置验证码宽度
		properties.put("kaptcha.textproducer.char.string", "1234567890");// 文本集合，验证码值从此集合中获取
		properties.put("kaptcha.noise.impl", "com.google.code.kaptcha.impl.NoNoise");// 去掉干扰线
		properties.put("kaptcha.textproducer.font.size", "20");// 设置字体大小
		properties.put("kaptcha.textproducer.font.names", "Arial");// 设置字体
		properties.put("kaptcha.word.impl", "com.google.code.kaptcha.text.impl.DefaultWordRenderer");// 文字渲染器
		Config config = new Config(properties);
		DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
		defaultKaptcha.setConfig(config);
		return defaultKaptcha;
	}
}
