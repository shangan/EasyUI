package org.colin.enums;

/**
 * @desc 返回状态码
 * @author wujiangbo
 * @date 2020年1月2日 上午10:53:07
 */
public enum ResultCode {
	/* 成功状态码 */
	SUCCESS("000", "成功"),
	/* 失败状态码 */
	FAIL("001", "失败"),
	/* 参数错误：10001-19999 */
	PARAM_IS_INVALID("10001", "参数无效"),
	PARAM_IS_BLANK("10002", "参数无效"),
	PARAM_TYPE_ERROR("10003", "参数类型错误"),
	PARAM_NOT_COMPLETE("10004", "参数缺失");

	private String code;

	private String message;

	private ResultCode(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String code() {
		return this.code;
	}

	public String message() {
		return this.message;
	}

}
