package org.colin.exceptions;

/**
 * @desc 无操作权限异常
 * @author wujiangbo
 * @date 2020年1月2日 下午4:21:30
 */
public class NoPermissionException extends RuntimeException {

	private static final long serialVersionUID = 381476893815541280L;

	public NoPermissionException(String msg) {
		super(msg);
	}
}
