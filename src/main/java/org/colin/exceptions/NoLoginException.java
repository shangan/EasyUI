package org.colin.exceptions;

/**
 * @desc 未登录时抛出此异常
 * @author wujiangbo
 * @date 2020年1月2日 下午4:16:20
 */
public class NoLoginException extends RuntimeException {

	private static final long serialVersionUID = -7158044853556609471L;

	public NoLoginException(String msg) {
		super(msg);
	}

}
