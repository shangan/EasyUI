package org.colin.exceptions;

/**
 * @desc 数据库异常
 * @author wujiangbo
 * @date 2020年1月2日 下午4:15:08
 */
public class DataDoException extends RuntimeException {

	private static final long serialVersionUID = 5132022072032816206L;

	public DataDoException(String msg) {
		super(msg);
	}

}
