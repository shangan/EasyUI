package org.colin.exceptions;

/**
 * @desc 文件不存在
 * @author wujiangbo
 * @date 2020年1月2日 下午4:20:28
 */
public class FileNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 8754269790170837260L;

	public FileNotFoundException(String msg) {
		super(msg);
	}

}
