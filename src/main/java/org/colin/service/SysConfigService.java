package org.colin.service;

import org.colin.model.ro.SysConfigRO;
import org.colin.model.vo.Result;

public interface SysConfigService {

	// 新增
	Result insert(SysConfigRO sysConfigRO);

	// 删除
	Result delete(String id);

	// 查询所有资源
	Result selectAll();

	// 更新
	Result update(SysConfigRO sysConfigRO);
}
