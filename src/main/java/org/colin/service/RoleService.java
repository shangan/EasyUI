package org.colin.service;

import org.colin.model.ro.RoleResourceRO;
import org.colin.model.ro.SysRoleRO;
import org.colin.model.vo.Result;
import org.colin.model.vo.SysRoleVO;

public interface RoleService {

	// 新增
	Result insert(SysRoleRO resource);

	// 删除
	Result delete(String id);

	// 查询所有
	Result<SysRoleVO> selectAll(SysRoleRO resource);

	// 更新
	Result update(SysRoleRO resource);

	// 修改角色权限
	Result updateRoleResource(RoleResourceRO roleResourceRO);

	// 根据角色ID查询所有资源信息
	Result selectResourceListByRoleId(String roleId);

}
