package org.colin.service.impl;

import java.util.List;
import org.colin.mapper.SysLogMapper;
import org.colin.model.ro.SysLogRO;
import org.colin.model.vo.Result;
import org.colin.model.vo.SysLogVO;
import org.colin.service.SystemLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SystemLogServiceImpl implements SystemLogService {

	@Autowired
	private SysLogMapper sysLogMapper;

	@Override
	public Result save(SysLogRO sysLogRO) {
		int result = sysLogMapper.insert(sysLogRO);
		if (result == 1) {
			return Result.success();
		} else {
			return Result.fail();
		}
	}

	@Override
	public Result deleteByPrimaryKey(String logId) {
		int result = sysLogMapper.deleteByPrimaryKey(logId);
		if (result == 1) {
			return Result.success();
		} else {
			return Result.fail();
		}
	}

	@Override
	public Result deleteBatch(String[] ids) {
		int result = sysLogMapper.deleteBatch(ids);
		if (result > 0) {
			return Result.success();
		} else {
			return Result.fail();
		}
	}

	@Override
	public Result selectAll(SysLogRO sysLogRO) {
		List<SysLogVO> list = sysLogMapper.selectAll(sysLogRO);
		int count = sysLogMapper.selectAllCount(sysLogRO);
		return Result.success(list, count, sysLogRO);
	}

	@Override
	public Result update(SysLogRO sysLogRO) {
		int result = sysLogMapper.update(sysLogRO);
		if (result == 1) {
			return Result.success();
		} else {
			return Result.fail();
		}
	}

}
