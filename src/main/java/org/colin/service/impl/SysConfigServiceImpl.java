package org.colin.service.impl;

import java.util.Date;
import java.util.List;
import org.colin.Tools.TokenUtil;
import org.colin.Tools.Tool;
import org.colin.mapper.SysConfigMapper;
import org.colin.model.ro.SysConfigRO;
import org.colin.model.vo.Result;
import org.colin.model.vo.SysConfigVO;
import org.colin.service.SysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysConfigServiceImpl implements SysConfigService {

	@Autowired
	private SysConfigMapper sysConfigMapper;

	@Autowired
	private TokenUtil tokenUtil;

	@Override
	public Result insert(SysConfigRO sysConfigRO) {
		sysConfigRO.setId(Tool.getPrimaryKey());
		sysConfigRO.setCreateTime(new Date());
		sysConfigRO.setUpdateTime(new Date());
		sysConfigRO.setUpdateUserId(tokenUtil.getRedisUser().getId());
		int result = sysConfigMapper.insert(sysConfigRO);
		if (result == 1) {
			return Result.success();
		} else {
			return Result.fail();
		}
	}

	@Override
	public Result delete(String id) {
		int result = sysConfigMapper.deleteByPrimaryKey(id);
		if (result == 1) {
			return Result.success();
		} else {
			return Result.fail();
		}
	}

	@Override
	public Result selectAll() {
		List<SysConfigVO> list = sysConfigMapper.selectAll();
		return Result.success(list);
	}

	@Override
	public Result update(SysConfigRO sysConfigRO) {
		sysConfigRO.setUpdateUserId(tokenUtil.getRedisUser().getId());
		int result = sysConfigMapper.updateByPrimaryKey(sysConfigRO);
		if (result == 1) {
			return Result.success();
		} else {
			return Result.fail();
		}
	}

}
