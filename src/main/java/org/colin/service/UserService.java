package org.colin.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.colin.model.entity.SysUser;
import org.colin.model.ro.LoginRO;
import org.colin.model.vo.Result;
import org.colin.model.vo.UserVO;

public interface UserService {

    // 新增
    Result insert(SysUser user);

    // 删除
    Result delete(String id);

    // 查询
    Result selectAll(SysUser user);

    // 查询所有客户
    void selectAllCust(HttpServletResponse httpServletResponse);

    // 更新
    Result update(SysUser user);

    // 系统登录
    UserVO login(LoginRO loginRO);

    // 系统注销
    Result loginOut();

    // 强制某用户下线
    Result forceLoginOut(String userId);
}
