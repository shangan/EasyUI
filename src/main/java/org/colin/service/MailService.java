package org.colin.service;

import org.colin.model.ro.EmailRO;

/**
 * 发送邮件接口
 */
public interface MailService {
    
    //发送普通邮件
    void sendSimpleMail(EmailRO emailRO);

    //发送Html邮件
    void sendHtmlMail(EmailRO emailRO);

    //发送带附件的邮件
    void sendAttachmentMail(EmailRO emailRO,String filePath);
}
