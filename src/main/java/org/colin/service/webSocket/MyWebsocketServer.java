package org.colin.service.webSocket;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArraySet;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import lombok.extern.slf4j.Slf4j;

@ServerEndpoint(value = "/websocket/{sid}")
@Component
@Slf4j
public class MyWebsocketServer {

	/**
	 * 建议使用在线websocket测试工具，很方便： http://www.bejson.com/httputil/websocket/
	 *
	 * 访问：ws://localhost:8000/se/websocket/123
	 */

	// 静态变量，用来记录当前在线连接数。应该把它设计成线程安全的。
	private static int onlineCount = 0;

	// concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。
	private static CopyOnWriteArraySet<MyWebsocketServer> webSocketSet = new CopyOnWriteArraySet<>();

	private static ConcurrentMap<String, MyWebsocketServer> webSocketMap = new ConcurrentHashMap<>();

	// 与某个客户端的连接会话，需要通过它来给客户端发送数据
	private Session session;

	// 接收sid
	private String sid = "";

	/**
	 * 连接建立成功调用的方法
	 */
	@OnOpen
	public void onOpen(Session session, @PathParam("sid") String sid) {
		this.session = session;
		// 加入set中
		webSocketSet.add(this);
		// 加入map中
		webSocketMap.put(sid, this);
		// 在线数加1
		addOnlineCount();
		log.info("有客户端[" + sid + "]上线,当前在线人数为" + getOnlineCount());
		this.sid = sid;
		sendMessage("你好，我们连接成功啦！");
	}

	/**
	 * 连接关闭调用的方法
	 */
	@OnClose
	public void onClose() {
		// 从set中删除
		webSocketSet.remove(this);
		// 从map中删除
		webSocketMap.remove(this.sid);
		// 修改用户在线状态
		// 在线数减1
		subOnlineCount();
		log.info("有一连接关闭！当前在线人数为" + getOnlineCount());
	}

	/**
	 * 收到客户端消息后调用的方法
	 *
	 * @param message 客户端发送过来的消息
	 */
	@OnMessage
	public void onMessage(String message, Session session) {
		log.info("收到来自客户端[" + sid + "]的信息:" + message);
		sendInfo("你好，我是机器人哦！", sid);
		// 群发消息
		/*
		 * for (MyWebsocketServer item : webSocketSet) { try {
		 * item.sendMessage(message); } catch (IOException e) { e.printStackTrace(); } }
		 */
	}

	/**
	 *
	 * @param session
	 * @param error
	 */
	@OnError
	public void onError(Session session, Throwable error) {
		log.error("websocket发生错误：{}", error);
	}

	/**
	 * 实现服务器主动推送
	 */
	public void sendMessage(String message) {
		try {
			this.session.getBasicRemote().sendText(message);
		} catch (IOException e) {
			log.error("websocket IO异常：{}", e);
		}
	}

	/**
	 * 群发自定义消息
	 *
	 * @param message 消息内容
	 * @param sid     消息对象id(为null则表示群发)
	 * @throws IOException
	 */
	public void sendInfo(String message, @PathParam("sid") String sid) {
		log.info("推送消息到客户端[" + sid + "]，推送内容:" + message);
		if (StringUtils.isNotBlank(sid)) {
			// 发送消息给指定对象
			MyWebsocketServer item = webSocketMap.get(sid);
			if (item != null) {
				item.sendMessage(message);
			}
			return;
		}
		// 群发消息到所有在线用户
		for (MyWebsocketServer item : webSocketSet) {
			item.sendMessage(message);
		}
	}

	/**
	 * 推送消息给指定的用户列表
	 *
	 * @param message 消息内容
	 * @param ids     用户id列表
	 */
	public void sendInfoToList(String message, String ids) {
		if (StringUtils.isEmpty(ids)) {
			return;
		}
		String[] strings = ids.split(",");
		for (String sid : strings) {
			MyWebsocketServer item = webSocketMap.get(sid);
			if (item != null) {
				item.sendMessage(message);
			}
		}
	}

	public static synchronized int getOnlineCount() {
		return onlineCount;
	}

	public static synchronized void addOnlineCount() {
		MyWebsocketServer.onlineCount++;
	}

	public static synchronized void subOnlineCount() {
		MyWebsocketServer.onlineCount--;
	}
}
