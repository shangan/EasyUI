package org.colin.service;

import org.colin.model.ro.SysResourceRO;
import org.colin.model.vo.Result;
import org.colin.model.vo.SysResourceVO;

public interface ResourceService {

	// 新增
	Result insert(SysResourceRO resource);

	// 删除
	Result delete(String id);

	// 查询所有资源
	Result<SysResourceVO> selectAll(SysResourceRO resource);

	// 更新
	Result update(SysResourceRO resource);

	// 菜单初始化
	Result menuInit();

	// 菜单初始化EasyUI
	Result menuInitEasyUI();
}
