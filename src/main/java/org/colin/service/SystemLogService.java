package org.colin.service;

import org.colin.model.ro.SysLogRO;
import org.colin.model.vo.Result;

public interface SystemLogService {

	// 新增
	Result save(SysLogRO sysLogRO);

	// 删除
	Result deleteByPrimaryKey(String logId);

	// 批量删除
	Result deleteBatch(String[] ids);

	// 查询所有记录
	Result selectAll(SysLogRO sysLogRO);

	Result update(SysLogRO sysLogRO);
}
