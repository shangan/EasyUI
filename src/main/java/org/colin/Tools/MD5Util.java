package org.colin.Tools;

import org.apache.commons.codec.digest.DigestUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * @desc MD5加密类
 * @author wujiangbo
 * @date 2020年1月2日 下午12:17:58
 */
@Slf4j
public class MD5Util {

	private final static String KEY = "893d4a24ff3a4fbd9c644b3fff8fa0a0";

	public static void main(String[] args) throws Exception {
		System.out.println(MD5Util.md5("123456"));
		System.out.println(MD5Util.md5("123456").length());
	}

	/**
	 * @desc 将字符串进行MD5加密
	 * @author wujiangbo
	 * @date 2020年1月2日 下午12:21:00
	 * @param text 明文
	 * @return String 密文
	 */
	public static String md5(String text) {
		String encodeStr = "";
		try {
			// 加密后的字符串
			encodeStr = DigestUtils.md5Hex(text + KEY);
		} catch (Exception e) {
			log.error("密码加密时发生异常:{}", e);
		}
		return encodeStr;
	}

}
