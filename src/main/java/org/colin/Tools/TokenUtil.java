package org.colin.Tools;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.colin.constant.Constants;
import org.colin.exceptions.NoLoginException;
import org.colin.model.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @desc token工具类
 * @author wujiangbo
 * @date 2020年1月3日 下午2:49:18
 */
@Component
public class TokenUtil {

	@Value("${system.tokenTimeout}")
	private Integer tokenTimeout;

	@Autowired
	private RedisUtil redisUtil;

	/**
	 * @desc 获取Redis服务器中的用户信息
	 * @author wujiangbo
	 * @date 2020年1月3日 下午2:52:50
	 * @return
	 */
	public UserVO getRedisUser() {
		// 获取当前用户token
		String headerToken = getToken();
		if (Tool.isBlank(headerToken)) {
			throw new NoLoginException("请求头head中token信息为空");
		}
		// 从Redis服务器中获取用户信息
		Map<Object, Object> map = redisUtil.hmget(Constants.REDIS_USER_INFO_KEY + headerToken);
		if (map == null) {
			throw new NoLoginException("用户信息为空，请重新登录");
		}
		UserVO user = (UserVO) map.get(Constants.MAP_USER_INFO_KEY);
		if (user == null) {
			throw new NoLoginException("用户信息为空，请重新登录");
		}
		return user;
	}

	/**
	 * @desc 获取请求头中的token值
	 * @author wujiangbo
	 * @date 2020年1月3日 下午2:49:58
	 * @return
	 */
	public String getToken() {
		ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = requestAttributes.getRequest();
		String token = request.getHeader("token");
		return token;
	}

}
