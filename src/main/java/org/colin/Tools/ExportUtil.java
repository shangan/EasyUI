package org.colin.Tools;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import lombok.extern.slf4j.Slf4j;

/**
 * @desc 导出工具类
 * @author wujiangbo
 * @date 2020年1月17日 下午5:56:33
 */
@Slf4j
public class ExportUtil {

	/**
	 * easypoi 导出
	 */
	public static void exportExcel(String excelName, Workbook workBook, HttpServletResponse response) {
		if (StringUtils.isBlank(excelName)) {
			excelName = "";
		}
		try {
			String fileName = excelName + "_" + System.currentTimeMillis() + ".xls";
			fileName = new String(fileName.getBytes(), "ISO8859-1");
			response.setContentType("application/octet-stream;charset=ISO8859-1");
			response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
			ServletOutputStream out = response.getOutputStream();
			try {
				workBook.write(out);
				out.flush();
			} catch (Exception e) {
				log.error("导出Excel-文件写入异常：{}", e.getMessage());
			} finally {
				out.close();
			}
		} catch (Exception e) {
			log.error("导出Excel异常：{}", e.getMessage());
		}
	}
}
