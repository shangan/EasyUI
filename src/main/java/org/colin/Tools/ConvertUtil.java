package org.colin.Tools;

/**
 * @desc 数据转换类
 * @author wujiangbo
 * @date 2020年1月4日 上午10:38:22
 */
public class ConvertUtil {

	public static void main(String[] args) {
	}

	/**
	 * @desc 根据省份得到省份所属地区
	 * @author wujiangbo
	 * @date 2020年1月4日 上午10:38:57
	 * @param province
	 * @return
	 */
	public static String convertProvince(String province) {
		String desc = "";
		// 西北地区(5个)：陕西省、甘肃省、青海省、宁夏回族自治区、新疆维吾尔自治区
		String s1 = "陕西省、甘肃省、青海省、宁夏回族自治区、新疆维吾尔自治区";
		// 西南地区(6个)：广西壮族自治区、四川省、贵州省、云南省、西藏自治区、重庆市
		String s2 = "广西壮族自治区、四川省、贵州省、云南省、西藏自治区、重庆市";
		// 华中地区(3个)：河南省、湖北省、湖南省
		String s3 = "河南省、湖北省、湖南省";
		// 华东地区(7个)：山东省、江苏省、安徽省、浙江省、江西省、台湾省、上海市
		String s4 = "山东省、江苏省、安徽省、浙江省、江西省、台湾省、上海市";
		// 华北地区(5个)：北京、天津、河北省、山西省、内蒙古自治区
		String s5 = "北京、天津、河北省、山西省、内蒙古自治区";
		// 东北地区(3个)：黑龙江省、辽宁省、吉林省
		String s6 = "黑龙江省、辽宁省、吉林省";
		// 华南地区(5个)：福建省、广东省、海南省、香港特别行政区、澳门特别行政区
		String s7 = "福建省、广东省、海南省、香港特别行政区、澳门特别行政区";
		// 共计34个
		if (s1.contains(province)) {
			desc = "西北地区";
		} else if (s2.contains(province)) {
			desc = "西南地区";
		} else if (s3.contains(province)) {
			desc = "华中地区";
		} else if (s4.contains(province)) {
			desc = "华东地区";
		} else if (s5.contains(province)) {
			desc = "华北地区";
		} else if (s6.contains(province)) {
			desc = "东北地区";
		} else if (s7.contains(province)) {
			desc = "华南地区";
		}
		return desc;
	}

}
