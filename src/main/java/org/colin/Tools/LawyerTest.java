package org.colin.Tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.util.PDFMergerUtility;
import org.colin.model.bean.LawerLetter;
import com.aspose.words.Document;
import lombok.extern.slf4j.Slf4j;

/**
 * @desc 律师函word文档转PDF测试类
 * @author wujiangbo
 * @date 2020年1月18日 上午10:56:55
 */
@Slf4j
public class LawyerTest {
	static int count = 5;// 测试文档个数

	public static void main(String[] args) throws InterruptedException {
		createWordTest();// 生成律师函word文档
		Thread.sleep(1000);
		word2Pdf();// 将律师函word文档转成pdf格式
		Thread.sleep(1000);
		mergePdf();// 将多个PDF文档合成一个
	}

	// 将多个PDF文件合成一个文档
	public static void mergePdf() {
		long begin = System.currentTimeMillis();
		try {
			List<InputStream> sourcesList = new ArrayList<InputStream>();
			PDFMergerUtility mergePdf = new PDFMergerUtility();
			File file = null;
			InputStream is = null;
			for (int i = 1; i <= count; i++) {
				// PDF合并工具类
				file = new File("D:/doc/out-" + i + ".pdf");
				is = new FileInputStream(file);
				sourcesList.add(is);
			}
			mergePdf.addSources(sourcesList);
			// 设置合并生成pdf文件名称
			mergePdf.setDestinationFileName("D:/doc/mergePdf11111111111.pdf");
			// 合并PDF
			mergePdf.mergeDocuments();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (COSVisitorException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		long end = System.currentTimeMillis();
		log.info("PDF文件合并完成，用时：{}秒", (end - begin) / 1000);
	}

	// 测试——生成律师函文档
	public static void createWordTest() {
		LawerLetter lawerLetter = null;
		DocxUtil du = new DocxUtil();
		for (int i = 1; i <= count; i++) {
			lawerLetter = new LawerLetter();
			lawerLetter.setCustName("李四" + i + "号");
			lawerLetter.setContractValidTime(DateUtil.parseDateToStr(new Date(), "yyyy年MM月dd日"));
			lawerLetter.setLoanAmount(new BigDecimal("55080.00"));
			lawerLetter.setLoanPeriod("10" + i);
			lawerLetter.setBeginTimeCase(DateUtil.parseDateToStr(new Date(), "yyyy年MM月dd日"));
			lawerLetter.setYuqiAmount(new BigDecimal("66335.59"));
			lawerLetter.setCurrentDate(DateUtil.parseDateToStr(new Date(), "yyyy年MM月dd日"));
			lawerLetter.setAdjudicationDate(DateUtil.parseDateToStr(new Date(), "yyyy年MM月dd日"));
			du.createLawerWord("D:/律师函模板-普通案件.docx", lawerLetter, "D:/doc/out-" + i + ".docx");
		}
	}

	// 测试——word转PDF
	public static void word2Pdf() {
		try {
			long time1 = System.currentTimeMillis();
			for (int i = 1; i <= count; i++) {
				// doc路径
				Document document = new Document("D:/doc/out-" + i + ".docx");
				// pdf路径
				File outputFile = new File("D:/doc/out-" + i + ".pdf");
				// 操作文档保存
				document.save(outputFile.getAbsolutePath(), com.aspose.words.SaveFormat.PDF);
			}
			long time2 = System.currentTimeMillis();
			log.info("DocxUtil(doc2Pdf)——word文件转换成PDF文件完成，耗时{}秒", (time2 - time1) / 1000);
		} catch (Exception e) {
			log.error("DocxUtil(doc2Pdf)——word文件转换成PDF文件——异常：{}", e);
		}
	}
}
