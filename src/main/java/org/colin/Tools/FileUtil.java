package org.colin.Tools;

import java.io.File;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import com.aliyun.oss.OSSClient;
import cn.afterturn.easypoi.excel.entity.TemplateExportParams;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class FileUtil {

	@Value("${oss.endpoint}")
	private String ossEndpoint;

	@Value("${oss.host}")
	private String ossUrlHost; // 上传文件路径

	@Value("${oss.accessKeyId}")
	private String ossAccessKeyId;

	@Value("${oss.accessKeySecret}")
	private String ossAccessKeySecret;

	@Value("${oss.bucket}")
	private String ossBucket;

	@Value("${system.fileMapping}")
	private String localFilePath;

	public TemplateExportParams getTemplateExportParams(String modelFileName) {
		Resource resource = new ClassPathResource("model" + File.separator + modelFileName);
		/**
		 * 有些系统提示找不到资源，可以把上面的代码换成下面这句： ClassPathResource resource = new
		 * ClassPathResource("model" + File.separator +modelFileName);
		 */
		File sourceFile;
		TemplateExportParams tep = new TemplateExportParams();
		try {
			sourceFile = resource.getFile();
			log.info("sourceFile.getAbsolutePath() = {}", sourceFile.getAbsolutePath());
			tep.setTemplateUrl(sourceFile.getAbsolutePath());
		} catch (IOException e) {
			log.error("FileUtil ---- getTemplateExportParams 异常：{}", e);
		}
		return tep;
	}

	/**
	 * @desc 删除阿里云Oss上的文件
	 * @author wujiangbo
	 * @date 2020年1月4日 上午9:13:59
	 * @param filePath
	 * @return
	 */
	public boolean deleteOssFile(String filePath) {
		// Endpoint以杭州为例，其它Region请按实际情况填写。
		String endpoint = ossEndpoint;
		// 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录
		// https://ram.console.aliyun.com 创建RAM账号。
		String accessKeyId = ossAccessKeyId;
		String accessKeySecret = ossAccessKeySecret;
		// 创建OSSClient实例。
		OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
		boolean uploadResult = true;
		try {
			ossClient.deleteObject(ossBucket, filePath);
		} catch (Exception e) {
			uploadResult = false;
			log.error("deleteOssFile方法异常：{}", e);
		} finally {
			// 关闭OSSClient。
			ossClient.shutdown();
		}
		return uploadResult;
	}

	/**
	 * @desc 上传文件到阿里云Oss
	 * @author wujiangbo
	 * @date 2020年1月4日 上午9:09:22
	 * @param file              File类型文件
	 * @param uploadOssFilePath 保存路径
	 * @return
	 */
	public String uploadFile2Oss(File file, String uploadOssFilePath) {
		// 获取到上传的文件名
		String fileOriginalName = file.getName();
		log.info("fileOriginalName={}", fileOriginalName);
		// 截取文件名，不带文件类型
		String fileName_temp = fileOriginalName.substring(0, fileOriginalName.lastIndexOf("."));
		// 截取文件后缀
		String suffix = fileOriginalName.substring(fileOriginalName.lastIndexOf("."));
		String fileName = fileName_temp + "_" + DateUtil.getCurrentTimeString();
		String fileFullName = uploadOssFilePath + fileName + suffix;
		boolean uploadResult = uploadFileOss(fileFullName, file);
		if (!uploadResult) {
			return "-1";
		}
		return ossUrlHost + fileFullName;
	}

	/**
	 * @desc 上传文件到阿里云Oss
	 * @author wujiangbo
	 * @date 2020年1月4日 上午9:09:22
	 * @param file              MultipartFile类型文件
	 * @param uploadOssFilePath 保存路径
	 * @return
	 */
	public String uploadMultipartFile2Oss(MultipartFile file, String uploadOssFilePath) {
		// 获取到上传的文件名
		String fileOriginalName = file.getOriginalFilename();
		log.info("fileOriginalName={}", fileOriginalName);
		// 截取文件名，不带文件类型
		String fileName_temp = fileOriginalName.substring(0, fileOriginalName.lastIndexOf("."));
		// 截取文件后缀
		String suffix = fileOriginalName.substring(fileOriginalName.lastIndexOf("."));
		String fileName = fileName_temp + "_" + DateUtil.getCurrentTimeString();
		String fileFullPath = uploadOssFilePath + fileName + suffix;
		boolean uploadResult = uploadMultipartFileOss(fileFullPath, file);
		if (!uploadResult) {
			return "-1";
		}
		return ossUrlHost + fileFullPath;
	}

	/**
	 * @desc 上传文件到Oss
	 * @author wujiangbo
	 * @date 2020年1月4日 上午9:11:15
	 * @param uploadOssFilePath
	 * @param file
	 * @return
	 */
	protected boolean uploadFileOss(String uploadOssFilePath, File file) {
		// Endpoint以杭州为例，其它Region请按实际情况填写。
		String endpoint = ossEndpoint;
		// 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录
		// https://ram.console.aliyun.com 创建RAM账号。
		String accessKeyId = ossAccessKeyId;
		String accessKeySecret = ossAccessKeySecret;
		// 创建OSSClient实例。
		OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
		boolean uploadResult = false;
		try {
			// 上传文件。<yourLocalFile>由本地文件路径加文件名包括后缀组成，例如/users/local/myfile.txt。
			ossClient.putObject(ossBucket, uploadOssFilePath, file);
			uploadResult = true;
		} catch (Exception e) {
			uploadResult = false;
			log.error("uploadFileOss方法发生异常：{}", e);
		} finally {
			// 关闭OSSClient。
			ossClient.shutdown();
		}
		return uploadResult;
	}

	/**
	 * @desc 上传文件到阿里云Oss
	 * @author wujiangbo
	 * @date 2020年1月4日 上午9:08:53
	 * @param uploadOssFilePath
	 * @param file
	 * @return
	 */
	protected boolean uploadMultipartFileOss(String uploadOssFilePath, MultipartFile file) {
		// Endpoint以杭州为例，其它Region请按实际情况填写。
		String endpoint = ossEndpoint;
		// 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录
		// https://ram.console.aliyun.com 创建RAM账号。
		String accessKeyId = ossAccessKeyId;
		String accessKeySecret = ossAccessKeySecret;
		// 创建OSSClient实例。
		OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
		boolean uploadResult = false;
		try {
			// 上传文件。<yourLocalFile>由本地文件路径加文件名包括后缀组成，例如/users/local/myfile.txt。
			ossClient.putObject(ossBucket, uploadOssFilePath, file.getInputStream());
			uploadResult = true;
		} catch (Exception e) {
			uploadResult = false;
			log.error("uploadMultipartFileOss方法发生异常：{}", e);
		} finally {
			// 关闭OSSClient。
			ossClient.shutdown();
		}
		return uploadResult;
	}

	/**
	 * @desc 删除上传到本地的文件
	 * @author wujiangbo
	 * @date 2020年1月4日 上午8:58:02
	 * @param filePath 上传到本地后返回的文件路径
	 * @return
	 */
	public boolean deleteLocalFile(String filePath) {
		boolean flag = false;
		// testFile\123\20201\测试_20200103182407.png
		if (!Tool.isBlank(filePath)) {
			// 拼接文件全路径
			String fileFullPath = localFilePath + File.separator + filePath;
			File file = new File(fileFullPath);
			flag = file.delete();
		}
		return flag;
	}

	/**
	 * @desc 上传文件到本地
	 * @author wujiangbo
	 * @date 2020年1月3日 下午5:53:10
	 * @param file
	 * @param filePath
	 * @return
	 */
	public String uploadFile2Local(MultipartFile file, String filePath) {
		String returnFilePath = "";
		try {
			// 获取到上传的文件名
			String fileOriginalName = file.getOriginalFilename();
			// 截取文件名，不带文件类型
			String fileName_temp = fileOriginalName.substring(0, fileOriginalName.lastIndexOf("."));
			// 截取文件后缀
			String suffix = fileOriginalName.substring(fileOriginalName.lastIndexOf("."));
			String fileName = fileName_temp + "_" + DateUtil.getCurrentTimeString();
			String yearMonth = DateUtil.getCurrentYearMonth();
			String file_temp = localFilePath + File.separator + filePath + File.separator + yearMonth;
			File fileTemp = new File(file_temp);
			if (!fileTemp.isDirectory()) {
				fileTemp.mkdirs();
			}
			String fileFullName = file_temp + File.separator + fileName + suffix;
			log.info("fileFullName={}", fileFullName);
			File targetFile = new File(fileFullName);
			// 保存文件
			file.transferTo(targetFile.getAbsoluteFile());
			returnFilePath = filePath + File.separator + yearMonth + File.separator + fileName + suffix;
		} catch (IllegalStateException e) {
			log.error("上传文件到本地，发生异常：{}", e);
		} catch (IOException e) {
			log.error("上传文件到本地，发生异常：{}", e);
		}
		return returnFilePath;
	}

	/**
	 * @desc 删除某个文件夹及所有子目录
	 * @author wujiangbo
	 * @date 2020年1月4日 下午12:23:21
	 * @param file 文件夹
	 * @return boolean
	 */
	public static boolean deleteFolder(File file) {
		boolean flag = false;
		try {
			File[] listFiles = file.listFiles();
			if (listFiles != null && listFiles.length > 0) {
				for (File file2 : listFiles) {
					deleteFolder(file2);
				}
			}
			flag = file.delete();
		} catch (Exception e) {
			log.error("deleteFolder发生异常：{}", e);
		}
		return flag;
	}

}
