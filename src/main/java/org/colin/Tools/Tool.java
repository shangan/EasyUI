package org.colin.Tools;

import cn.hutool.core.lang.ObjectId;
import cn.hutool.core.util.IdcardUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Decoder;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author wujiangbo
 * @desc 工具类
 * @date 2020年1月2日 下午12:13:26
 */
@Slf4j
public class Tool {

    public static void main(String[] args) {
        IdWorker worker1 = new IdWorker(1, 1, 1);
        System.out.println(worker1.nextId());
    }

    /**
     * @param imgStr Base64数据(包含头信息)字符串
     * @return MultipartFile
     * @desc 将图片Base64数据(包含头信息)字符串转成MultipartFile文件类型
     * @author wujiangbo
     * @date 2020年1月16日 上午10:01:53
     */
    public static MultipartFile base64MutipartFile(String imgStr) {
        try {
            // Base64数据头：data:image/png;base64,
            String[] baseStr = imgStr.split(",");
            BASE64Decoder base64Decoder = new BASE64Decoder();
            byte[] b = new byte[0];
            b = base64Decoder.decodeBuffer(baseStr[1]);
            for (int i = 0; i < b.length; ++i) {
                if (b[i] < 0) {
                    b[i] += 256;
                }
            }
            return new BASE64DecodedMultipartFile(b, baseStr[0]);
        } catch (Exception e) {
            log.error("图片Base64串转MultipartFile发生异常:{}", e);
            return null;
        }
    }

    /**
     * @param fieldName
     * @param object
     * @return
     * @desc 通过反射根据属性名获取属性值
     * @author wujiangbo
     * @date 2020年1月15日 下午6:02:28
     */
    public static Object getFieldValueByName(String fieldName, Object object) {
        try {
            String firstLetter = fieldName.substring(0, 1).toUpperCase();
            String getter = "get" + firstLetter + fieldName.substring(1);
            Method method = object.getClass().getMethod(getter, new Class[]{});
            Object value = method.invoke(object, new Object[]{});
            return value;
        } catch (Exception e) {
            log.error("通过反射根据属性名获取属性值——发生异常:{}", e);
            return null;
        }
    }

    /**
     * @Description: 根据身份证号获取性别、出生日期、年龄、省份等信息
     * @MethodName: getInfoByIdCard
     * @param: [idCard]
     * @return: java.util.Map<java.lang.String, java.lang.Object>
     * @Author: wujiangbo
     * @Date: 2020-01-17 17:35
     */
    public static Map<String, String> getInfoByIdCard(String idCard) {
        Map<String, String> map = new HashMap<String, String>();
        if (isBlank(idCard) || !IdcardUtil.isValidCard(idCard)) {
            return map;
        }
        int sex_code = IdcardUtil.getGenderByIdCard(idCard);// 性别（0：女；1：男；）
        map.put("sex", sex_code == 0 ? "女" : "男");
        map.put("birthday", IdcardUtil.getYearByIdCard(idCard) + "-" + IdcardUtil.getMonthByIdCard(idCard) + "-" + IdcardUtil.getDayByIdCard(idCard));
        map.put("age", String.valueOf(IdcardUtil.getAgeByIdCard(idCard)));
        map.put("province", IdcardUtil.getProvinceByIdCard(idCard));
        return map;
    }

    /**
     * @return String
     * @desc 获取49位长度的UUID(当前时间 + UUID)
     * @author wujiangbo
     * @date 2020年1月3日 下午2:19:08
     */
    public static String getUUID49() {
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        return df.format(new Date()) + UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * @param str
     * @return
     * @desc 判断字符串是否为空
     * @author wujiangbo
     * @date 2020年1月2日 下午3:20:24
     */
    public static boolean isBlank(String str) {
        return StringUtils.isBlank(str);
    }

    /**
     * @return
     * @desc 获取主键ID，与业务无关
     * @author wujiangbo
     * @date 2020年1月2日 下午12:13:49
     */
    public static String getPrimaryKey() {
        return ObjectId.next();
    }
}
