package org.colin.Tools.AudioUtil;

/**
 * @desc FFMPEGLocator
 * @author wujiangbo
 * @date 2019年12月30日 下午4:11:22
 */
public abstract class FFMPEGLocator {
	/**
	 * This method should return the path of a ffmpeg executable suitable for the
	 * current machine.
	 *
	 * @return The path of the ffmpeg executable.
	 */
	protected abstract String getFFMPEGExecutablePath();

	/**
	 * It returns a brand new {@link FFMPEGExecutor}, ready to be used in a ffmpeg
	 * call.
	 *
	 * @return A newly instanced {@link FFMPEGExecutor}, using this locator to call
	 *         the ffmpeg executable.
	 */
	FFMPEGExecutor createExecutor() {
		return new FFMPEGExecutor(getFFMPEGExecutablePath());
	}
}
