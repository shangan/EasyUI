package org.colin.Tools.AudioUtil;

/**
 * @desc InputFormatException
 * @author wujiangbo
 * @date 2019年12月30日 下午4:11:46
 */
public class InputFormatException extends EncoderException {

	private static final long serialVersionUID = 1L;

	InputFormatException() {
		super();
	}

	InputFormatException(String message) {
		super(message);
	}
}
