package org.colin.Tools.AudioUtil;

/**
 * @desc EncoderException
 * @author wujiangbo
 * @date 2019年12月30日 下午4:10:54
 */
public class EncoderException extends Exception {

	private static final long serialVersionUID = 1L;

	EncoderException() {
		super();
	}

	EncoderException(String message) {
		super(message);
	}

	EncoderException(Throwable cause) {
		super(cause);
	}

	EncoderException(String message, Throwable cause) {
		super(message, cause);
	}
}
