package org.colin.Tools.AudioUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;

/**
 * @desc RBufferedReader
 * @author wujiangbo
 * @date 2019年12月30日 下午4:12:03
 */
public class RBufferedReader extends BufferedReader {
	/**
	 * Re-inserted lines buffer.
	 */
	private ArrayList lines = new ArrayList();

	/**
	 * It builds the reader.
	 *
	 * @param in The underlying reader.
	 */
	public RBufferedReader(Reader in) {
		super(in);
	}

	/**
	 * It returns the next line in the stream.
	 */
	@Override
	public String readLine() throws IOException {
		if (lines.size() > 0) {
			return (String) lines.remove(0);
		} else {
			return super.readLine();
		}
	}

	/**
	 * Reinserts a line in the stream. The line will be returned at the next
	 * {@link RBufferedReader#readLine()} call.
	 *
	 * @param line The line.
	 */
	public void reinsertLine(String line) {
		lines.add(0, line);
	}
}
