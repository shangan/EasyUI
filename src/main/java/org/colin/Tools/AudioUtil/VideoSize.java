package org.colin.Tools.AudioUtil;

import java.io.Serializable;

/**
 * @desc VideoSize
 * @author wujiangbo
 * @date 2019年12月30日 下午4:12:17
 */
public class VideoSize implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * The video width.
	 */
	private int width;

	/**
	 * The video height.
	 */
	private int height;

	/**
	 * It builds the bean.
	 *
	 * @param width  The video width.
	 * @param height The video height.
	 */
	public VideoSize(int width, int height) {
		this.width = width;
		this.height = height;
	}

	/**
	 * Returns the video width.
	 *
	 * @return The video width.
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Returns the video height.
	 *
	 * @return The video height.
	 */
	public int getHeight() {
		return height;
	}

	@Override
	public String toString() {
		return getClass().getName() + " (width=" + width + ", height=" + height + ")";
	}
}
