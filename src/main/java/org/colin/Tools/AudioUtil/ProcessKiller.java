package org.colin.Tools.AudioUtil;

/**
 * @desc ProcessKiller
 * @author wujiangbo
 * @date 2019年12月30日 下午4:11:58
 */
public class ProcessKiller extends Thread {
	/**
	 * The process to kill.
	 */
	private Process process;

	/**
	 * Builds the killer.
	 *
	 * @param process The process to kill.
	 */
	public ProcessKiller(Process process) {
		this.process = process;
	}

	/**
	 * It kills the supplied process.
	 */
	@Override
	public void run() {
		process.destroy();
	}
}
