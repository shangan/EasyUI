package org.colin.Tools.AudioUtil;

import java.io.BufferedReader;
import java.io.File;
import lombok.extern.slf4j.Slf4j;

/**
 * @desc 忽略所有转换过程中的异常
 * @author wujiangbo
 * @date 2019年12月30日 下午4:05:37
 */
@Slf4j
public class IgnoreErrorEncoder extends Encoder {

	@Override
	protected void processErrorOutput(EncodingAttributes attributes, BufferedReader errorReader, File source, EncoderProgressListener listener) {
		try {
			String line;
			while ((line = errorReader.readLine()) != null) {
				log.debug(line);
			}
		} catch (Exception exp) {
			log.error("file convert error message process failed. ", exp);
		}
	}
}
